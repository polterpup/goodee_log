<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<%
		request.setCharacterEncoding("utf-8");

		if (request.getParameter("boardNo") == null) {
			response.sendRedirect("./selectBoardList.jsp");
			return;
		}

		int boardNo = Integer.parseInt(request.getParameter("boardNo"));
		System.out.println(boardNo + " <-- boardNo");

		BoardDao boardDao = new BoardDao();
		ArrayList<String> boardCategoryList = boardDao.selectBoardCategoryList();
		%>
		<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
			<a class="navbar-brand btn btn-warning" href="./selectBoardList.jsp">전체보기</a>
			<%
			for (String category : boardCategoryList) {
			%>
			<a class="navbar-brand"	href="./selectBoardList.jsp?boardCategory=<%=category%>"><%=category%></a>
			<%
			}
			%>
		</nav>
		<div class="jumbotron">
			<h1>게시글 상세보기</h1>
		</div>
		<%
		Board board = boardDao.selectBoardOne(boardNo);
		%>
		<div>
			<table class="table table-dark">
				<tr>
					<td>board_no</td>
					<td>
						<div><%=boardNo%></div>
						<div>
							<a class="btn-secondary"
								href="./updateBoardForm.jsp?boardNo=<%=boardNo%>">수정</a>
						</div>
						<div>
							<a class="btn-secondary"
								href="./deleteBoard.jsp?boardNo=<%=boardNo%>">삭제</a>
						</div>
					</td>
				</tr>
				<tr>
					<td>board_category</td>
					<td><%=board.getBoardCategory()%></td>
				</tr>
				<tr>
					<td>board_title</td>
					<td><%=board.getBoardTitle()%></td>
				</tr>
				<tr>
					<td>board_content</td>
					<td><%=board.getBoardContent()%></td>
				</tr>
				<tr>
					<td>board_date</td>
					<td><%=board.getBoardDate()%></td>
				</tr>
			</table>
		</div>
		<!-- 댓글 partial -->
		<h3>댓글입력</h3>
		<div>
			<form action="./insertCommentAction.jsp" method="post">
				<input type="hidden" name="boardNo" value="<%=boardNo%>">
				<div class="form-group">
					<label for="comment">Comment:</label>
					<textarea name="commentContent" class="form-control" rows="5"></textarea>
				</div>
				<button class="btn btn-warning" type="submit">댓글입력</button>
			</form>
		</div>
		<h3>댓글목록</h3>
		<div>
			<!-- 댓글목록 출력, 10개씩 페이징 -->
			<%
			CommentDao commentDao = new CommentDao();
			ArrayList<Comment> commentList = commentDao.selectCommentListByPage(boardNo, 1, 10);

			for (Comment c : commentList) {
			%>
			<div class="bg-dark text-white">
				<%=c.getCommentContent()%>
				<a
					href="./deleteComment.jsp?commentNo=<%=c.getCommentNo()%>&boardNo=<%=boardNo%>" class="btn-secondary">삭제</a>
				<!-- commentNo의 덧글을 삭제 후 다시 이 페이지 되돌아오기(redirect) 위해서 boardNo 넘겨준다 -->
			</div>
			<%
			}
			%>
			<div>
				<a href="">이전</a> <a href="">다음</a>
			</div>

		</div>
	</div>
</body>
</html>
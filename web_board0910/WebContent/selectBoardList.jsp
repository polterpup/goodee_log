<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "vo.Board" %>
<%@ page import = "dao.BoardDao" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<%
	request.setCharacterEncoding("utf-8");
	int currentPage = 1;
	if(request.getParameter("currentPage") != null) { 
		currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}
	
	String boardCategory = "";
	if(request.getParameter("boardCategory") != null) {
		boardCategory = request.getParameter("boardCategory");
	}
	// boardCategory는 null or "csharp", "css", ....
	// null이면 전체에서 select
	// null아 아니면 boardCategory만 select
 	
	
	BoardDao boardDao = new BoardDao();
	ArrayList<String> boardCategoryList = boardDao.selectBoardCategoryList();
	
	BoardDao boardDao2 = new BoardDao();
	ArrayList<String> boardCategoryList2 = boardDao2.selectBoardCategoryList();
%>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a class="navbar-brand btn btn-warning" href="./selectBoardList.jsp">전체보기</a>
		<%
			for(String category : boardCategoryList) {
		%>
				<a class="navbar-brand" href="./selectBoardList.jsp?boardCategory=<%=category%>"><%=category%></a>
		<%		
			}
		%>
	</nav>
	
	<div>
		<!-- 본문내용 -->
		<%
			ArrayList<Board> boardList = boardDao.selectBoardListByPage(currentPage, 10, boardCategory);
		%>
		
		<div class="jumbotron">
			<h1>전체 게시글 목록</h1>
			<p>
				csharp, html, java,...
				<div><a href="./insertBoardForm.jsp" class="btn btn-warning">글입력</a></div>
			</p>
		</div>
		
		<table class="table table-dark table-striped">
			<thead>
				<tr>
					<th>board_category</th>
					<th>board_title</th>
				</tr>
			</thead>
			<tbody>
				<%
					for(Board b : boardList) {
				%>
						<tr>
							<td><%=b.getBoardCategory()%></td>
							<!-- 제목링크 상세보기.... -->
							<td><a href="./selectBoardOne.jsp?boardNo=<%=b.getBoardNo()%>"><%=b.getBoardTitle()%></a></td>
						</tr>
				<%		
					}
				%>
			</tbody>
		</table>
		<div>
			<a class="btn btn-warning" href="./selectBoardList.jsp?currentPage=<%=currentPage-1%>&boardCategory=<%=boardCategory%>"">이전</a>
			<a class="btn btn-warning" href="./selectBoardList.jsp?currentPage=<%=currentPage+1%>&boardCategory=<%=boardCategory%>">다음</a>
		</div>
	</div>
</div>
</body>
</html>




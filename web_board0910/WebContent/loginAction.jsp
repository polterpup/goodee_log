<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%
	// request 값 디버깅
	String memberId = request.getParameter("memberId");
	String memberPw = request.getParameter("memberPw");
	
	MemberDao memberDao = new MemberDao();
	Member paramMember = new Member();
	
	paramMember.setMemberId(memberId);
	paramMember.setMemberPw(memberPw);
	
	// 성공시 : null이 아닌 값 리턴
	Member returnMember = memberDao.login(paramMember);
	
	// Debug
	if(returnMember == null) {
		System.out.println("로그인 실패");
		response.sendRedirect("./loginForm.jsp");
		return;
	} else {
		System.out.println("로그인 성공");
		System.out.println("[ID] " + returnMember.getMemberId() + "\t[Name] " + returnMember.getMemberName());
		
		// request, session : JSP 내장객체
		// 한 사용자의 공간(session)에 변수를 생성
		session.setAttribute("loginMember", returnMember);
		response.sendRedirect("./index.jsp");
		
	}
%>
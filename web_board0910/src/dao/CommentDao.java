package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import db.DBUtil;
import vo.Comment;

public class CommentDao {
	public ArrayList<Comment> selectCommentListByPage(int boardNo, int commentCurrentPage, int commentRowPerPage) throws ClassNotFoundException, SQLException {
		ArrayList<Comment> list = new ArrayList<>();
		int comentBegingRow = (commentCurrentPage-1) * commentRowPerPage;
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		String sql = "select comment_no, comment_content from comment where board_no=? order by comment_date desc limit ?,?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, boardNo);
		stmt.setInt(2, comentBegingRow);
		stmt.setInt(3, commentRowPerPage);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			Comment c = new Comment();
			c.setCommentNo(rs.getInt("comment_no"));
			c.setCommentContent(rs.getString("comment_content"));
			list.add(c);
		}
		return list;
	}
}

package dao;

import java.sql.*;
import java.util.*;

import db.DBUtil;
import vo.Board;

public class BoardDao {

	
	public Board selectBoardOne(int boardNo) throws ClassNotFoundException, SQLException {
		Board board = null;
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		String sql = "select board_category, board_title, board_content, board_date from board where board_no=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, boardNo);
		ResultSet rs = stmt.executeQuery();
		if(rs.next()) {
			board = new Board();
			board.setBoardCategory(rs.getString("board_category"));
			board.setBoardTitle(rs.getString("board_title"));
			board.setBoardContent(rs.getString("board_content"));
			board.setBoardDate(rs.getString("board_date"));
		}
		return board;
	}
	
	public int selectLastPage(int rowPerPage) throws ClassNotFoundException, SQLException {
		int lastPage = 0;
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		String sql = "SELECT COUTN(*) from board";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		int totalRowCount = 0;
		if(rs.next()) {
			totalRowCount = rs.getInt("COUTN(*)");
		}
		lastPage = totalRowCount / rowPerPage;
		if(totalRowCount % rowPerPage != 0) {
			lastPage++;
		}
		return lastPage;
	}
	
	public ArrayList<Board> selectBoardListByPage(int currentPage, int rowPerPage,String boardCategory) throws ClassNotFoundException, SQLException {
		int beginRow = (currentPage-1)*rowPerPage;
		ArrayList<Board> boardList = new ArrayList<>();	
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		String sql = null;
		PreparedStatement stmt = null;
		if(boardCategory.equals("") == true) { 
			sql = "select board_no, board_category, board_title from board order by board_date desc limit ?,?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, beginRow);
			stmt.setInt(2, rowPerPage);
		} else {
			sql = "select board_no, board_category, board_title from board where board_category=? order by board_date desc limit ?,?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, boardCategory);
			stmt.setInt(2, beginRow);
			stmt.setInt(3, rowPerPage);
		}
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			Board board = new Board();
			board.setBoardNo(rs.getInt("board_no"));
			board.setBoardCategory(rs.getString("board_category"));
			board.setBoardTitle(rs.getString("board_title"));
			boardList.add(board);
		}
		rs.close();
		stmt.close();
		conn.close();
		return boardList;
	}
	
	public ArrayList<String> selectBoardCategoryList() throws ClassNotFoundException, SQLException {
		ArrayList<String> boardCategoryList = new ArrayList<>();	
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		String sql = "select distinct board_category from board order by board_category asc";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		while(rs.next()) {
			boardCategoryList.add(rs.getString("board_category"));
		}
		rs.close();
		stmt.close();
		conn.close();
		return boardCategoryList;
	}
}

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<!DOCTYPE html>
	<html>

	<head>
		<meta charset="UTF-8">
		<title>testForm</title>

	</head>

	<body>
		<div class="container">
			<form action="./testAction.jsp">
				<h1>정보처리기사 필기</h1>

				<fieldset>
					<legend>1. 검토회의 전에 요구사항 명세서를 미리 배포하여 사전 검토한 후 짧은 검토 회의를 통해 오류를 조기에 검출하는데 목적을 두는 요구 사항 검토 방법은?
					</legend>
					<input type="radio" name="question1" value="1"> 1. 빌드검증<br>
					<input type="radio" name="question1" value="2"> 2. 동료 검토<br>
					<input type="radio" name="question1" value="3"> 3. 워크 스루<br>
					<input type="radio" name="question1" value="4"> 4. 개발자 검토<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>2. 코드 설계에서 일정한 일련번호를 부여하는 방식의 코드는?</legend>
					<input type="radio" name="question2" value="1"> 1. 연상 코드<br>
					<input type="radio" name="question2" value="2"> 2. 블록 코드<br>
					<input type="radio" name="question2" value="3"> 3. 순차 코드<br>
					<input type="radio" name="question2" value="4"> 4. 표의 숫자 코드<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>3. 객체지향 프로그램에서 데이터를 추상화하는 단위는?</legend>
					<input type="radio" name="question3" value="1"> 1. 메소드<br>
					<input type="radio" name="question3" value="2"> 2. 클래스<br>
					<input type="radio" name="question3" value="3"> 3. 상속성<br>
					<input type="radio" name="question3" value="4"> 4. 메시지<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>4. 데이터 흐름도(DFD)의 구성요소에 포함되지 않는 것은?</legend>
					<input type="radio" name="question4" value="1"> 1. process<br>
					<input type="radio" name="question4" value="2"> 2. data flow<br>
					<input type="radio" name="question4" value="3"> 3. data store<br>
					<input type="radio" name="question4" value="4"> 4. data dictionary<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>5. 소프트웨어 설계시 구축된 플랫폼의 성능특성 분석에 사용되는 측정 항목이 아닌 것은?</legend>
					<input type="radio" name="question5" value="1"> 1. 응답시간(Response Time)<br>
					<input type="radio" name="question5" value="2"> 2. 가용성(Availability)<br>
					<input type="radio" name="question5" value="3"> 3. 사용률(Utilization)<br>
					<input type="radio" name="question5" value="4"> 4. 서버 튜닝(Server Tuning)<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>6. UML 확장 모델에서 스테레오 타입 객체를 표현할 때 사용하는 기호로 맞는 것은?</legend>
					<input type="radio" name="question6" value="1"> 1. &lt;&lt; &gt;&gt;<br>
					<input type="radio" name="question6" value="2"> 2. (( ))<br>
					<input type="radio" name="question6" value="3"> 3. {{ }}<br>
					<input type="radio" name="question6" value="4"> 4. [[ ]]<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>7. GoF(Gang of Four)의 디자인 패턴에서 행위 패턴에 속하는 것은?</legend>
					<input type="radio" name="question7" value="1"> 1. Builder<br>
					<input type="radio" name="question7" value="2"> 2. Visitor<br>
					<input type="radio" name="question7" value="3"> 3. Prototype<br>
					<input type="radio" name="question7" value="4"> 4. Bridge<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>8. 자료 사전에서 자료의 생략을 의미하는 기호는?</legend>
					<input type="radio" name="question8" value="1"> 1. {}<br>
					<input type="radio" name="question8" value="2"> 2. **<br>
					<input type="radio" name="question8" value="3"> 3. =<br>
					<input type="radio" name="question8" value="4"> 4. ( )<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>9. 트랜잭션이 올바르게 처리되고 있는지 데이터를 감시하고 제어하는 미들웨어는?</legend>
					<input type="radio" name="question9" value="1"> 1. RPC<br>
					<input type="radio" name="question9" value="2"> 2. ORB<br>
					<input type="radio" name="question9" value="3"> 3. TP monitor<br>
					<input type="radio" name="question9" value="4"> 4. HUB<br>
				</fieldset>
				<br>
				<fieldset>
					<legend>10. UI 설계 원칙에서 누구나 쉽게 이해하고 사용할 수 있어야 한다는 것은?</legend>
					<input type="radio" name="question10" value="1"> 1. 유효성<br>
					<input type="radio" name="question10" value="2"> 2. 직관성<br>
					<input type="radio" name="question10" value="3"> 3. 무결성<br>
					<input type="radio" name="question10" value="4"> 4. 유연성<br>
				</fieldset>

				<br>
				<button type="submit">답안전송</button>
				<br>
			</form>
		</div>
	</body>

	</html>
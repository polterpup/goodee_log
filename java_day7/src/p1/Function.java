package p1;

// 기능의 컨테이너 역할 클래스
public class Function {
	public void greet() {
		System.out.println("hello");
	}

	public void print(int x) {
		System.out.println(x);
	}

	public int add(int x, int y) {
		int z = x + y;
		return z;
	}
	
	public static void hi() {
		System.out.println("hi");
	}
}

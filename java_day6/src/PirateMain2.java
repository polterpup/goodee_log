import java.util.*;

public class PirateMain2 {
	public static void main(String[] args) {
		Pirate p1 = new Pirate();
		p1.name = "몽키D.루피";
		p1.age = 19;
		p1.gender = "남";
		p1.blood = "F";
		p1.teamName = "밀짚모자해적단";
		p1.devilFruit = true;
		p1.wanted = 1500000000L;
		p1.home = "이스트블루";

		Pirate p2 = new Pirate();
		p2.name = "롤로노아조로";
		p2.age = 22;
		p2.gender = "남";
		p2.blood = "XF";
		p2.teamName = "밀짚모자해적단";
		p2.devilFruit = false;
		p2.wanted = 320000000L;
		p2.home = "이스트블루";

		Pirate p3 = new Pirate();
		p3.name = "나미";
		p3.age = 21;
		p3.gender = "여";
		p3.blood = "X";
		p3.teamName = "밀짚모자해적단";
		p3.devilFruit = false;
		p3.wanted = 66000000;
		p3.home = "이스트블루";

		Pirate p4 = new Pirate();
		p4.name = "우솝";
		p4.age = 21;
		p4.gender = "남";
		p4.blood = "S";
		p4.teamName = "밀짚모자해적단";
		p4.devilFruit = false;
		p4.wanted = 200000000L;
		p4.home = "이스트블루";

		Pirate p5 = new Pirate();
		p5.name = "빈스모크상디";
		p5.age = 21;
		p5.gender = "남";
		p5.blood = "S";
		p5.teamName = "밀짚모자해적단";
		p5.devilFruit = false;
		p5.wanted = 330000000L;
		p5.home = "이스트블루";

		Pirate p6 = new Pirate();
		p6.name = "토니토니쵸파";
		p6.age = 7;
		p6.gender = "남";
		p6.blood = "X";
		p6.teamName = "밀짚모자해적단";
		p6.devilFruit = true;
		p6.wanted = 100;
		p6.home = "위대한항로";

		Pirate p7 = new Pirate();
		p7.name = "니코로빈";
		p7.age = 27;
		p7.gender = "여";
		p7.blood = "S";
		p7.teamName = "밀짚모자해적단";
		p7.devilFruit = true;
		p7.wanted = 130000000L;
		p7.home = "웨스트블루";

		Pirate p8 = new Pirate();
		p8.name = "프랑키";
		p8.age = 30;
		p8.gender = "남";
		p8.blood = "XF";
		p8.teamName = "밀짚모자해적단";
		p8.devilFruit = false;
		p8.wanted = 94000000;
		p8.home = "사우스블루";

		Pirate p9 = new Pirate();
		p9.name = "브룩";
		p9.age = 19;
		p9.gender = "남";
		p9.blood = "X";
		p9.teamName = "밀짚모자해적단";
		p9.devilFruit = true;
		p9.wanted = 83000000;
		p9.home = "웨스트블루";

		Pirate p10 = new Pirate();
		p10.name = "징베";
		p10.age = 45;
		p10.gender = "남";
		p10.blood = "F";
		p10.teamName = "밀짚모자해적단";
		p10.devilFruit = false;
		p10.wanted = 438000000L;
		p10.home = "어인섬";

		Pirate p11 = new Pirate();
		p11.name = "샹크스";
		p11.age = 37;
		p11.gender = "남";
		p11.blood = "XF";
		p11.teamName = "빨간머리해적단";
		p11.devilFruit = false;
		p11.wanted = 4048900000L;
		p11.home = "웨스트블루";

		// Pirate[] p; → java.util.ArrayList<Pirate> p;
		ArrayList<Pirate> list = new ArrayList<>();
		list.add(p1);
		list.add(p2);
		list.add(p3);
		list.add(p4);
		list.add(p5);
		list.add(p6);
		list.add(p7);
		list.add(p8);
		list.add(p9);
		list.add(p10);
		list.add(p11);

		System.out.println(list.size() + "명\n");

		// for문
		for (int i = 0; i < list.size(); i++) {
			System.out.print(list.get(i).name + " ");
		}
		System.out.println("");

		// foreach문
		for (Pirate p : list) {
			System.out.print(p.name + " ");
		}

		// 4. 혈액형별 분류
		ArrayList<Pirate> bloodX = new ArrayList<>();
		ArrayList<Pirate> bloodF = new ArrayList<>();
		ArrayList<Pirate> bloodXF = new ArrayList<>();
		ArrayList<Pirate> bloodS = new ArrayList<>();

		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).blood.equals("X")) {
				bloodX.add(list.get(i));
			} else if (list.get(i).blood.equals("F")) {
				bloodF.add(list.get(i));
			} else if (list.get(i).blood.equals("XF")) {
				bloodXF.add(list.get(i));
			} else {
				bloodS.add(list.get(i));
			}
		}

		System.out.println("\n\n4-1 X형 출력");
		for (int i = 0; i < bloodX.size(); i++) {
			System.out.println(
					bloodX.get(i).name + "(" + bloodX.get(i).blood + ")");
		}
		System.out.println("\n\n4-2 F형 출력");
		for (int i = 0; i < bloodF.size(); i++) {
			System.out.println(
					bloodF.get(i).name + "(" + bloodF.get(i).blood + ")");
		}
		System.out.println("\n\n4-3 XF형 출력");
		for (int i = 0; i < bloodXF.size(); i++) {
			System.out.println(
					bloodXF.get(i).name + "(" + bloodXF.get(i).blood + ")");
		}
		System.out.println("\n\n4-4 S형 출력");
		for (int i = 0; i < bloodS.size(); i++) {
			System.out.println(
					bloodS.get(i).name + "(" + bloodS.get(i).blood + ")");
		}

		// 5. 나이의 합계
		int ageSum = 0;

		for (Pirate p : list) {
			ageSum += p.age;
		}

		System.out.println("\n5. 나이의 합계 : " + ageSum + "세");

		// 6. 나이의 평균
		System.out.println("6. 나이의 평균 : " + (ageSum / list.size()) + "세");

		// 7. 나이 최대값
		int ageMax = Integer.MIN_VALUE;
		int ageMin = Integer.MAX_VALUE;

		for (Pirate p : list) {
			if (ageMax < p.age) {
				ageMax = p.age;
			}

			if (ageMin > p.age) {
				ageMin = p.age;
			}
		}

		System.out.println("7. 나이 최대값 : " + ageMax + "세");

		// 8. 나이 최소값
		System.out.println("8. 나이 최소값 : " + ageMin + "세\n");

		// 9. 고향이 이스트블루 또는 웨스트블루인 해적 중 가장 현상금 높은 해적의 이름과 현상금은?
		long maxBounty = 0;
		String topPirate = "";

		for (Pirate p : list) {
			if (p.home.equals("이스트블루") || p.home.equals("웨스트블루")) {
				if (maxBounty < p.wanted) {
					maxBounty = p.wanted;
					topPirate = p.name;
				}
			}
		}

		System.out.println("이스트블루 출신 혹은 웨스트블루 출신 해적 중 가장 현상금이 높은 해적 : "
				+ topPirate + " [" + maxBounty + " 베리]");

	}
}

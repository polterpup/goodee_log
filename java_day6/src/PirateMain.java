
public class PirateMain {
	public static void main(String[] args) {
		Pirate p1 = new Pirate();
		p1.name = "몽키D.루피";
		p1.age = 19;
		p1.gender = "남";
		p1.blood = "F";
		p1.teamName = "밀짚모자해적단";
		p1.devilFruit = true;
		p1.wanted = 1500000000L;
		p1.home = "이스트블루";

		Pirate p2 = new Pirate();
		p2.name = "롤로노아조로";
		p2.age = 22;
		p2.gender = "남";
		p2.blood = "XF";
		p2.teamName = "밀짚모자해적단";
		p2.devilFruit = false;
		p2.wanted = 320000000L;
		p2.home = "이스트블루";

		Pirate p3 = new Pirate();
		p3.name = "나미";
		p3.age = 21;
		p3.gender = "여";
		p3.blood = "X";
		p3.teamName = "밀짚모자해적단";
		p3.devilFruit = false;
		p3.wanted = 66000000;
		p3.home = "이스트블루";

		Pirate p4 = new Pirate();
		p4.name = "우솝";
		p4.age = 21;
		p4.gender = "남";
		p4.blood = "S";
		p4.teamName = "밀짚모자해적단";
		p4.devilFruit = false;
		p4.wanted = 200000000L;
		p4.home = "이스트블루";

		Pirate p5 = new Pirate();
		p5.name = "빈스모크상디";
		p5.age = 21;
		p5.gender = "남";
		p5.blood = "S";
		p5.teamName = "밀짚모자해적단";
		p5.devilFruit = false;
		p5.wanted = 330000000L;
		p5.home = "이스트블루";

		Pirate p6 = new Pirate();
		p6.name = "토니토니쵸파";
		p6.age = 7;
		p6.gender = "남";
		p6.blood = "X";
		p6.teamName = "밀짚모자해적단";
		p6.devilFruit = true;
		p6.wanted = 100;
		p6.home = "위대한항로";

		Pirate p7 = new Pirate();
		p7.name = "니코로빈";
		p7.age = 27;
		p7.gender = "여";
		p7.blood = "S";
		p7.teamName = "밀짚모자해적단";
		p7.devilFruit = true;
		p7.wanted = 130000000L;
		p7.home = "웨스트블루";

		Pirate p8 = new Pirate();
		p8.name = "프랑키";
		p8.age = 30;
		p8.gender = "남";
		p8.blood = "XF";
		p8.teamName = "밀짚모자해적단";
		p8.devilFruit = false;
		p8.wanted = 94000000;
		p8.home = "사우스블루";

		Pirate p9 = new Pirate();
		p9.name = "브룩";
		p9.age = 19;
		p9.gender = "남";
		p9.blood = "X";
		p9.teamName = "밀짚모자해적단";
		p9.devilFruit = true;
		p9.wanted = 83000000;
		p9.home = "웨스트블루";

		Pirate p10 = new Pirate();
		p10.name = "징베";
		p10.age = 45;
		p10.gender = "남";
		p10.blood = "F";
		p10.teamName = "밀짚모자해적단";
		p10.devilFruit = false;
		p10.wanted = 438000000L;
		p10.home = "어인섬";

		Pirate p11 = new Pirate();
		p11.name = "샹크스";
		p11.age = 37;
		p11.gender = "남";
		p11.blood = "XF";
		p11.teamName = "빨간머리해적단";
		p11.devilFruit = false;
		p11.wanted = 4048900000L;
		p11.home = "웨스트블루";

		Pirate[] pirate = {p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11};
		// System.out.println(pirate[10].name);

		// for 문
		for (int i = 0; i < pirate.length; i++) {
			System.out.println(i + 1 + ") " + pirate[i].name + ", "
					+ pirate[i].home + ", " + pirate[i].teamName);
		}

		// foreach 문 → for(복사본 : 집합체)
		for (Pirate p : pirate) {
			System.out.println(p.name + ", " + p.home + ", " + p.teamName);
		}

		// 1) 전체인원 (count 통계)
		System.out.println(pirate.length + "명 ← 1. 전체인원");

		// 2) 밀짚모자해적단의 인원
		int strawHatCount = 0;
		for (int i = 0; i < pirate.length; i++) {
			if (pirate[i].teamName.equals("밀짚모자해적단")) {
				strawHatCount++;
			}
		}
		System.out.println(strawHatCount + "명 ← 2. 밀짚모자해적단의 인원");

		// 3) 밀짚모자해적단중 열매 비능력자 출력 & 몇명? (count 통계)
		int notDevilFruitCount = 0;
		for (Pirate p : pirate) {
			if (p.teamName.equals("밀짚모자해적단") && (p.devilFruit == false)) {
				System.out.print(p.name + " ");
				notDevilFruitCount++;
			}
		}
		System.out.println(
				"\n" + notDevilFruitCount + "명 ← 3. 밀짚모자해적단중 열매 비능력자 인원");
		
	}
}

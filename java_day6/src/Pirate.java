public class Pirate {
	// JAVA : 필드 | C, C++ : 멤버 변수
	public String name; // 이름
	public int age; // 나이
	public String gender; // 성별
	public String blood; // 혈액형
	public String teamName; // 해적단이름
	public boolean devilFruit; // 악마의 열매 섭취 유무
	public long wanted; // 현상금 ( bounty )
	public String home; // 고향
}

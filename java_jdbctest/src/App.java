import java.sql.*;

public class App {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("org.mariadb.jdbc.Driver");
		System.out.println("Driver클래스 로딩 성공"); // Debug
		
		String dburl = "jdbc:mariadb://127.0.0.1:3307/test";
		String dbuser = "root";
		String dbpw = "java1004";
		
		Connection conn = DriverManager.getConnection(dburl, dbuser, dbpw);
		System.out.println("conn Debug : " + conn); // Debug
		
		String sql = "SELECT * FROM member";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		
		while(rs.next()) {
			System.out.print(rs.getString("member_id") + "\t");
			System.out.print(rs.getString("member_pw") + "\t");
			System.out.println(rs.getString("member_name"));
		}
	}

}

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<style type="text/css">
		form {
			text-align: center;
			width: 280px;
		}
		
		input {
			text-align: center;
		}
	
	</style>
</head>
<body>
	<h1>등장인물 입력</h1>
	<form action="./insertPirateAction.jsp">
		<table border="1">
			<tr>
				<th>이름</th>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<th>나이</th>
				<td><input type="text" name="age"></td>
			</tr>
			<tr>
				<th>성별</th>
				<td><input name="gender" type="radio" value="남"> 남 <input
					name="gender" type="radio" value="여"> 여</td>
			</tr>
			<tr>
				<th>혈액형</th>
				<td><select name="blood">
						<option value="X">X</option>
						<option value="F">F</option>
						<option value="XF">XF</option>
						<option value="S">S</option>
				</select></td>
			</tr>
			<tr>
				<th>소속 해적단</th>
				<td><input type="text" name="team_name"></td>
			</tr>
			<tr>
				<th>악마의 열매 섭취 유/무</th>
				<td><input name="devil_fruit" type="radio" value="true">
					유 <input name="devil_fruit" type="radio" value="false"> 무</td>
			</tr>
			<tr>
				<th>현상금</th>
				<td><input type="text" name="bounty" value="0"></td>
			</tr>
			<tr>
				<th>고향</th>
				<td><input type="text" name="home"></td>
			</tr>
		</table>
		<button type="submit" style="margin: 10px 0 0 0; float: right;">입력</button>
	</form>
</body>
</html>
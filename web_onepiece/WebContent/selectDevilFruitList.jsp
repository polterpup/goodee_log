<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ page import="java.sql.ResultSet"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h1>악마의 열매 목록</h1>
	<%
	int currentPage = 1;
	if (request.getParameter("currentPage") != null) {
		currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}

	int rowPerPage = 10; // 보여질 갯수
	int beginRow = (currentPage - 1) * rowPerPage; // 시작점 

	// 1) Mysql Database를 사용할 수 있도록 실행
	// Class 클래스 안에 forName(문자열) 정적(static) 메서드로 정의
	Class.forName("org.mariadb.jdbc.Driver");

	// 2) 원격에 존재하는 Mysql Database 연결기능 실행 Static 메서드 DriverManger.getConnection
	// -> 연결정보 저장 시 사용하는 데이터 타입 (Connection)
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/onepiece", "root", "java1004");

	// 3) 쿼리를 만들어서 쿼리를 실행
	/* 
		select * form devil_fruit order by name limit 0, 10;
	*/

	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM devil_fruit order by name limit ?, ?");
	stmt.setInt(1, beginRow); // 첫 번째 ? 인자
	stmt.setInt(2, rowPerPage); // 두 번째 ? 인자

	// Debug 필수!!
	// System.out.println(stmt + " <-- stmt");

	ResultSet rs = stmt.executeQuery();
	%>

	<table border="1" cellpadding="5" cellspacing="5"
		style="text-align: center;">
		<thead>
			<tr>
				<th>계통(category)</th>
				<th>악마의 열매 이름</th>
				<th>능력자</th>
				<th>번호</th>
			</tr>
		</thead>
		<tbody>
			<%
			int num = 1 + beginRow;
			while (rs.next()) { // while(rs.next() == true)
			%>
			<tr>
				<td><%=num%></td>
				<td><%=rs.getString("category")%></td>
				<td><%=rs.getString("name")%></td>
				<td><%=rs.getString("person")%></td>
			</tr>
			<%
			num++;
			}
			%>
		</tbody>
	</table>
	<%
	if (currentPage > 1) {
	%>
	<a href="./selectDevilFruitList.jsp?currentPage=<%=currentPage - 1%>">이전</a>
	<%
	}
	int totalCount = 0;
	PreparedStatement stmt2 = conn.prepareStatement("select count(*) from devil_fruit");
	ResultSet rs2 = stmt2.executeQuery();
	
	if (rs2.next()) {
		totalCount = rs2.getInt("count(*)");
	}

	int lastPage = totalCount / rowPerPage;
	
	if (totalCount % rowPerPage != 0) {
	lastPage += 1; // lastPage++; lastPage=lastPage+1;
	}

	if (currentPage < lastPage) {
	%>
	<a href="./selectDevilFruitList.jsp?currentPage=<%=currentPage + 1%>">다음</a>
	<%
	}
	%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h1>원피스 프로젝트</h1>
	<img alt="" src="./image/onepiece.jpg" width="640" height="320">
	<h2>[악마의 열매 관리]</h2>
	<ol>
		<li><a href="./insertDevilFruitForm.jsp" style="text-decoration: unset;">악마의 열매 추가(insert)</a></li>
		<li><a href="./selectDevilFruitList.jsp" style="text-decoration: unset;">악마의 열매 목록(select)</a></li>
	</ol>
	<h2>[등장인물 관리]</h2>
	<ol>
		<li><a href="./insertPirateForm.jsp" style="text-decoration: unset;">등장인물 추가(insert)</a></li>
		<li><a href="./selectPirateList.jsp" style="text-decoration: unset;">등장인물 목록(select)</a></li>
	</ol>
</body>
</html>
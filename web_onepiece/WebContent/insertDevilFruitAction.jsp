<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.DriverManager"%>
<%@ page import="java.sql.Connection"%>
<%@ page import="java.sql.PreparedStatement"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	String category = request.getParameter("category");
	String name = request.getParameter("name");
	String person = request.getParameter("person");

	// 1) MariaDB (Mysql)를 사용하기 위해서....
	Class.forName("org.mariadb.jdbc.Driver"); // 입력값 : 클래스의 풀네임을 문자열로
	System.out.println("mariadb 사용가능");

	// 2) MariaDB 접속
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/onepiece", "root", "java1004");
	System.out.println(conn);

	// 3) Connection에서 쿼리 실행자를 호출
	PreparedStatement stmt = conn.prepareStatement("insert into devil_fruit(name, category, person) values(?, ?, ?)"); // ? 표현식 : 변수자리

	// 3-2) 쿼리 표현식 완		
	stmt.setString(1, name);
	stmt.setString(2, category); // DB에 들어가는 순으로 입력
	stmt.setString(3, person);

	// 4) 쿼리 실행
	stmt.executeUpdate();
	%>
	<script>
		location.href = "insertDevilFruitForm.jsp";
	</script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h1>등장인물 목록</h1>
	<%
	int currentPage = 1;
	if (request.getParameter("currentPage") != null) {
		currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}

	int rowPerPage = 10; // 보여질 갯수
	int beginRow = (currentPage - 1) * rowPerPage; // 시작점 

	// 1) Mysql Database를 사용할 수 있도록 실행
	// Class 클래스 안에 forName(문자열) 정적(static) 메서드로 정의
	Class.forName("org.mariadb.jdbc.Driver");

	// 2) 원격에 존재하는 Mysql Database 연결기능 실행 Static 메서드 DriverManger.getConnection
	// -> 연결정보 저장 시 사용하는 데이터 타입 (Connection)
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/onepiece", "root", "java1004");

	// 3) 쿼리를 만들어서 쿼리를 실행
	/* 
		select * form devil_fruit order by name limit 0, 10;
	*/

	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM pirate order by id limit ?, ?");
	stmt.setInt(1, beginRow); // 첫 번째 ? 인자
	stmt.setInt(2, rowPerPage); // 두 번째 ? 인자

	// Debug 필수!!
	// System.out.println(stmt + " <-- stmt");

	ResultSet rs = stmt.executeQuery();
	%>

	<table border="1" cellpadding="5" cellspacing="5"
		style="text-align: center;">
		<thead>
			<tr>
				<th>순번</th>
				<th>이름</th>
				<th>나이</th>
				<th>성별</th>
				<th>혈액형</th>
				<th>소속 해적단</th>
				<th>악마의 열매 섭취 유/무</th>
				<th>현상금</th>
				<th>고향</th>
			</tr>
		</thead>
		<tbody>
			<%
			while (rs.next()) { // while(rs.next() == true)
			%>
			<tr>
				<td><%=rs.getInt("id")%></td>
				<td><%=rs.getString("name")%></td>
				<td><%=rs.getInt("age")%> 세</td>
				<td>
					<%
					if (rs.getString("gender").equals("남")) {
					%> <img src="./image/male.png" width="30" height="30"> <%} else {%>
					<img src="./image/female.png" width="30" height="30"> <%}%>
				</td>

				<td><%=rs.getString("blood")%></td>
				<td><%=rs.getString("team_name")%></td>
				<td>
					<%
					if (rs.getString("devil_fruit").equals("true")) {
					%> <img src="./image/fruit.png" width="30" height="30"> <%}%>
				</td>
				<td><%=rs.getLong("bounty")%> 베리</td>
				<td><%=rs.getString("home")%></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
	<%
	if (currentPage > 1) {
	%>
	<a href="./selectPirateList.jsp?currentPage=<%=currentPage - 1%>">이전</a>
	<%
	}
	int totalCount = 0;
	PreparedStatement stmt2 = conn.prepareStatement("select count(*) from pirate");
	ResultSet rs2 = stmt2.executeQuery();

	if (rs2.next()) {
	totalCount = rs2.getInt("count(*)");
	}

	int lastPage = totalCount / rowPerPage;

	if (totalCount % rowPerPage != 0) {
	lastPage += 1; // lastPage++; lastPage=lastPage+1;
	}

	if (currentPage < lastPage) {
	%>
	<a href="./selectPirateList.jsp?currentPage=<%=currentPage + 1%>"
		style="margin-left: 770px;">다음</a>
	<%
	}
	%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h1>악마의 열매 입력</h1>
	<form action="./insertDevilFruitAction.jsp" style="width: 245px">
		<table border="1">
			<tr>
				<th>카테고리</th>
				<td><select name="category">
						<option value="초인계">초인계</option>
						<option value="동물계">동물계</option>
						<option value="자연계">자연계</option>
						<option value="계통불명">계통불명</option>
				</select></td>
			</tr>
			<tr>
				<th>열매의 이름</th>
				<td><input type="text" name="name"></td>
			</tr>
			<tr>
				<th>능력자</th>
				<td><input type="text" name="person"></td>
			</tr>
		</table>
		<button type="submit" style="margin: 10px 0 0 10px; float: right;">입력</button>
	</form>
</body>
</html>
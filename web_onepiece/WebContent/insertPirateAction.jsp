<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	String name = request.getParameter("name");
	int age = Integer.parseInt(request.getParameter("age"));
	String gender = request.getParameter("gender");
	String blood = request.getParameter("blood");
	String teamname = request.getParameter("team_name");
	String devilfruit = request.getParameter("devil_fruit");
	long bounty = Long.parseLong(request.getParameter("bounty"));
	String home = request.getParameter("home");

	// Debug Code
	/* 
	System.out.print(name + " " + age + " " + gender + " " + blood 
			+ " " + team_name + " " + devil_fruit + " " + bounty 
			+ " " + home);
	*/

	// 1) MariaDB (Mysql)를 사용하기 위해서....
	Class.forName("org.mariadb.jdbc.Driver"); // 입력값 : 클래스의 풀네임을 문자열로
	System.out.println("mariadb 사용가능");

	// 2) MariaDB 접속
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/onepiece", "root", "java1004");
	System.out.println(conn);

	// 3) Connection에서 쿼리 실행자를 호출
	PreparedStatement stmt = conn.prepareStatement(
			"insert into pirate(name, age, gender, blood, team_name, devil_fruit, bounty, home) values(?, ?, ?, ?, ?, ?, ?, ?)"); // ? 표현식 : 변수자리

	// 3-2) 쿼리 표현식 완		
	stmt.setString(1, name);
	stmt.setInt(2, age); // DB에 들어가는 순으로 입력
	stmt.setString(3, gender);
	stmt.setString(4, blood);
	stmt.setString(5, teamname);
	stmt.setString(6, devilfruit);
	stmt.setLong(7, bounty);
	stmt.setString(8, home);

	// 4) 쿼리 실행
	stmt.executeUpdate();
	%>

	<script>
		alert("저장되었습니다.");
		window.location.href = "./insertPirateForm.jsp";
	</script>
</body>
</html>
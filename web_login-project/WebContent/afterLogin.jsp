<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	//  이 웹사이트의 모든사람들이 이용가능하기 때문에 사용X
	// String loginValue = (String)application.getAttribute("loginValue");
	
	String loginValue = (String)session.getAttribute("loginValue");
	if(loginValue == null ) { // yes가 아닐때
		response.sendRedirect("./beforeLogin.jsp");
		return;
	}
	%>
	<h1>로그인 사용자만 사용 가능한 페이지입니다</h1>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>board 글입력</h1>
	<form action="./insertBoardAction.jsp" method="post">
		<div>boardPw :</div>
		<div>
			<input type="password" name="boardPw" id="boardPw">
		</div>
		<div>boardTitle :</div>
		<div>
			<input type="text" name="boardTitle" id="boardTitle">
		</div>
		<div>boardContent :</div>
		<div>
			<textarea name="boardContent" id="boardContent" rows="5" cols="50"></textarea>
		</div>
		<div>boardName :</div>
		<div>
			<input type="text" name="boardUser" id="boardUser">
		</div>
		<div>
			<button type="submit">글입력</button>
			<button type="reset">초기화</button>
		</div>
	</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*" %>
<%@ page import = "vo.Board" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	request.setCharacterEncoding("utf-8");
	
	//	방어코드

	// selectBoardList에서 넘겨받은 boardNo 저장
	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	System.out.println("boardNo : "+boardNo);
	
	/*
	SELECT board_no, board_title, board_content, board_user, board_date 
	FROM board 
	where board_no=?
	*/
	
	// 1. 데이터 수집 (DB에서 수집된 데이터는 ResultSet의 모양)
	Class.forName("org.mariadb.jdbc.Driver");
	// MySql TCP port:3306 / MariaDB TCP port:3307
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/webboard","root","java1004");
	// 쿼리를 만들어서 쿼리를 실행
	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM board WHERE board_no=?");
	stmt.setInt(1, boardNo);
	// 디버깅 코드 : stmt에 쿼리내용과 표현식의 파라미터값 확인가능
	System.out.println("stmt : "+stmt);
		
	ResultSet rs = stmt.executeQuery();
	
	// 2. 데이터 가공 (ResultSet --> Board[] or ArrayList<Board>)
	// ResultSet는 값을 한 행 단위로 불러오는 클래스인데 참조타입으로 만들어서 JAVA에서 사용하기 쉽게 해줌
	// 자료구조화, 아주 아주 중요!
	// Board board = null; : 불필요한 객체 생성으로 낭비 막아줌
	Board board = null;
	if(rs.next()){
		// new 연산자로 객체 생성시 변수는 0,null,false로 자동 초기화
		board = new Board();
		// board 초기화 : SELECT한 rs를 board에 초기화
		board.boardNo = rs.getInt("board_no");
		board.boardTitle = rs.getString("board_title");
		board.boardContent = rs.getString("board_content");
		board.boardUser = rs.getString("board_user");
		board.boardDate = rs.getString("board_date");
	}
	
	// 3. board 출력
%>
	<table width="700px">
		<tr>
			<td colspan="2"><%=board.boardNo%></td>
		</tr>
		<tr>
			<td colspan="2">
				<h1><%=board.boardTitle%></h1>
			</td>
		</tr>
		<tr>
			<td>
				<h2><%=board.boardUser%></h2>
			</td>
			<td><%=board.boardDate%></td>
		</tr>
		<tr>
			<td colspan="2">
				<fieldset><%=board.boardContent%></fieldset>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<!-- 1. 비밀번호 입력폼 | 2. action에서 확인 후 삭제 -->
				<a href="./deleteBoardForm.jsp?boardNo=<%=board.boardNo%>">삭제</a>
				<!-- 1. 수정할 폼 (값을 받아서 채워져 있어야 함) | 2. action에서 비밀번호 확인 후 수정-->
				<a href="./updateBoardForm.jsp?boardNo=<%=board.boardNo%>">수정</a>
			</td>
		</tr>
	</table>

</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "vo.Board" %>
<%@ page import = "java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<% 
	// 0. 설정
	// 메소드 방식이 post이기 때문에 인코딩 방식을 명시해줘야 함
	request.setCharacterEncoding("utf-8");
	
	// 1. 데이터 수집
	// 입력 값 받아서 변수에 저장 
	String boardPw = request.getParameter("boardPw");
	String boardTitle = request.getParameter("boardTitle");
	String boardContent = request.getParameter("boardContent");
	String boardUser = request.getParameter("boardUser");
	
	/*
	if (boardPw == null || boardTitle == null || boardContent == null || boardUser == null) {
		response.sendRedirect("./insertBoardForm.jsp");
		return; // 메서드를 종료하는 명령어
	}
	*/
	
	// 디버깅 코드
	System.out.println("boardPw : "+boardPw);
	System.out.println("boardTitle : "+boardTitle);
	System.out.println("boardContent : "+boardContent);
	System.out.println("boardUser : "+boardUser);
	
	// 2. 데이터 가공 (자료구조)
	// new 연산자로 객체 생성시 변수는 0,null,false로 자동 초기화
	Board board = new Board();
	// board 초기화 : 입력받은 값들을 board에 초기화
	board.boardPw = boardPw;
	board.boardTitle = boardTitle;
	board.boardContent = boardContent;
	board.boardUser = boardUser;
	
	// 3. 이행(DB에 입력)
	// 3-1) MariaDB 사용 : 이 페이지에서 마리아 데이터 베이스를 사용할 수 있도록 실행
	Class.forName("org.mariadb.jdbc.Driver");
	// 디버깅 코드
	System.out.println("mariadb Driver 등록 성공!");
	// 3-2) MariaDB 접속
	// MySql TCP port:3306 / MariaDB TCP port:3307
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.01:3307/webboard","root","1801");
	// 디버깅 코드
	System.out.println("conn : "+conn);
	
	// 3-3) 쿼리생성, 실행
	// insert문에 now()를 사용하여 현재시각을 불러온뒤, board_date에 저장
	PreparedStatement stmt = conn.prepareStatement("insert into board(board_pw, board_title, board_content, board_user, board_date) values(?,?,?,?,now())");
	stmt.setString(1, board.boardPw);
	stmt.setString(2, board.boardTitle);
	stmt.setString(3, board.boardContent);
	stmt.setString(4, board.boardUser);
	// 디버깅 코드 : stmt에 쿼리내용과 표현식의 파라미터값 확인가능
	System.out.println("stmt : "+stmt);
	
	// 3-4) 쿼리 실행
	int row = stmt.executeUpdate();
	
	if (row == 1) {
		response.sendRedirect("./selectBoardList.jsp?currentPage=1");

	} else {
		System.out.println("잘못된 입력입니다.");
	}
%>
</body>
</html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="vo.Board"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>BORAD LIST</h1>
	<!-- 
	리스트로 만들어서 페이징 하기
	SELECT board_no, board_title, board_user FROM board 
	ORDER BY board_date desc limit ?,?
-->
	<%
	// 페이지번호 = currentPage를 1로 디폴트
	int currentPage = 1;
	// current가 null이 아니라면 값을 int 타입으로로 바꾸어서 페이지 번호로 씀
	if (request.getParameter("currentPage") != null) {
		currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}
	// 디버깅
	System.out.println("currentPage : " + currentPage);
	// limit 값 설정 beginRow부터 rowPerPage까지 보여주세요
	int rowPerPage = 10;
	int beginRow = (currentPage - 1) * rowPerPage;

	// 1. 데이터 수집 (DB에서 수집된 데이터는 ResultSet의 모양)
	Class.forName("org.mariadb.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/webboard", "root", "java1004");

	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM board ORDER BY board_no desc limit ?,?");
	stmt.setInt(1, beginRow);
	stmt.setInt(2, rowPerPage);
	// 디버깅 : stmt에 쿼리내용과 표현식의 파라미터값 확인가능
	System.out.println("stmt : " + stmt);

	ResultSet rs = stmt.executeQuery();

	// 2. 데이터 가공 (ResultSet --> Board[] or ArrayList<Board>)
	// ResultSet는 값을 한 행 단위로 불러오는 클래스인데 ArrayList로 만들어서 JAVA에서 사용하기 쉽게 해줌
	// 자료구조화, 아주 아주 중요!
	// new 연산자로 객체 생성시 변수는 0,null,false로 자동 초기화
	ArrayList<Board> list = new ArrayList<>();
	while (rs.next()) {
		// board 초기화 : SELECT한 rs를 board에 초기화
		Board b = new Board();
		b.boardNo = rs.getInt("board_no");
		b.boardTitle = rs.getString("board_title");
		b.boardUser = rs.getString("board_user");
		list.add(b);
	}

	// 3. 이행(출력) - 가공된 데이터를 이용
	%>
	<table border="1">
		<thead>
			<tr>
				<th>글번호</th>
				<th>제목</th>
				<th>작성자</th>
			</tr>
		</thead>
		<tbody>
			<%
			for (Board b : list) {
			%>
			<tr>
				<td><%=b.boardNo%></td>
				<td><a href="./selectBoardOne.jsp?boardNo=<%=b.boardNo%>"><%=b.boardTitle%></a></td>
				<td><%=b.boardUser%></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
	<%
	// 전체 행의 수
	int totalCount = 0;
	PreparedStatement stmt2 = conn.prepareStatement("SELECT count(*) FROM board");
	ResultSet rs2 = stmt2.executeQuery();
	if (rs2.next()) {
		totalCount = rs2.getInt("count(*)");
	}
	// 디버깅
	System.out.println("totalCount : " + totalCount);

	// 마지막 페이지
	// lastPage를 전체 행의 수와 한 페이지에 보여질 행의 수(rowPerPage)를 이용하여 구한다
	int lastPage = totalCount / rowPerPage;
	if (totalCount % rowPerPage != 0) {
		lastPage += 1;
	}
	// 디버깅
	System.out.println("lastPage : " + lastPage);

	// 화면에 보여질 페이지 번호의 갯수
	int displayPage = 10;

	// 화면에 보여질 시작 페이지 번호
	// ((현재페이지번호 - 1) / 화면에 보여질 페이지 번호) * 화면에 보여질 페이지 번호 + 1
	// (currentPage - 1)을 하는 이유는 현재페이지가 10일시에도 startPage가 1이기 위해서
	int startPage = ((currentPage - 1) / displayPage) * displayPage + 1;
	// 디버깅
	System.out.println("startPage : " + startPage);

	// 화면에 보여질 마지막 페이지 번호
	// 화면에 보여질 시작 페이지 번호 + 화면에 보여질 페이지 번호 - 1
	// -1을 하는 이유는 페이지 번호의 갯수가 10개이기 때문에 statPage에서 더한 1을 빼준다
	int endPage = startPage + displayPage - 1;

	// 디버깅
	System.out.println("endPage : " + endPage);

	// 이전 버튼
	// 화면에 보여질 시작 페이지 번호가 화면에 보여질 페이지 번호의 갯수보다 크다면 이전 버튼을 생성
	if (startPage > displayPage) {
	%>
	<a href="./selectBoardList.jsp?currentPage=<%=startPage - displayPage%>">이전</a>
	<%
	}

	// 페이지 번호 버튼
	// 화면에 보여질 시작 페이지 번호를 화면에 보여질 마지막 페이지 번호까지 반복하면서 페이지 번호 생성
	// 만약에 화면에 보여질 마지막 페이지 번호가 마지막 페이지보다 크다면 for문을 break로 종료시킴
	for (int i = startPage; i <= endPage; i++) {
	if (endPage < lastPage) {
	%>
	<a href="./selectBoardList.jsp?currentPage=<%=i%>"><%=i%></a>
	<%
	} else if (endPage > lastPage) {
	%>
	<a href="./selectBoardList.jsp?currentPage=<%=i%>"><%=i%></a>
	<%
	break;
		}
	}

	// 다음 버튼
	// 화면에 보여질 마지막 페이지 번호가 마지막페이지보다 작다다면 이전 버튼을 생성
	if (endPage < lastPage) {
	%>
	<a
		href="./selectBoardList.jsp?currentPage=<%=startPage + displayPage%>">다음</a>
	<%
	}
	%>
	<br>
	<a href="./insertBoardForm.jsp">글쓰기</a>
</body>
</html>

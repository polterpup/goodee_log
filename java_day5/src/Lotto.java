
public class Lotto {

	public static void main(String[] args) {
		// 요구사항분석

		int[] ball = new int[45]; // index 0~44

		// 초기화
		for (int i = 0; i < ball.length; i++) {
			ball[i] = i + 1;
		}

		// 1) 디버깅
//		for (int i = 0; i < ball.length; i++) {
//			System.out.print(ball[i] + " ");
//		}

		for (int i = 0; i < 1000; i++) {
			int r = (int) (Math.random() * 45);
			int temp = ball[0];
			ball[0] = ball[r];
			ball[r] = temp;
		}
		
		for (int i=0; i<6; i++) {
			System.out.print(ball[i] + " ");
		}
	}

}

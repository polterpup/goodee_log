
public class Ex3 {

	public static void main(String[] args) {
		String[] strawHat = null;
		strawHat = new String[10];
		strawHat[0] = "루피";
		strawHat[1] = "조로";
		strawHat[2] = "나미";
		strawHat[3] = "우솝";
		strawHat[4] = "상디";
		strawHat[5] = "쵸파";
		strawHat[6] = "로빈";
		strawHat[7] = "프랑키";
		strawHat[8] = "브룩";
		strawHat[9] = "징베";

		// 개발자가 사고하는 방식
		int i = 0;
		while (i <= 9) {
			System.out.println(strawHat[i]);
			i = i + 1;
		}

		// 개발자가 코드로 옮기는 방식
		for (int j = 0; j < strawHat.length; j++) {
			System.out.println(strawHat[j]);
		}
	}

}

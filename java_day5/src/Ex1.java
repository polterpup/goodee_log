public class Ex1 {
	public static void main(String[] args) {
		// 배열 : 여러개 (동일한 타입)의 타입.
		int[] arr = { 3, 6, 0 };

		System.out.println(arr);

		String str = "구디";
		System.out.println(str);
		
		// 정식표
		int[] arr2 = new int[] { 3, 6, 0 };
		String str2 = new String("구디");
	}
}
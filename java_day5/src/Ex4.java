import java.io.FileInputStream;

public class Ex4 {
	public static void main(String[] args) throws Exception {
		// 1) 자료수집
		FileInputStream fis = new FileInputStream("d:\\temp\\score.txt");
		System.out.println(fis);
		int num = 0;
		String score = "";

		// 2) 자료구조
		while ((num = fis.read()) != -1) {
			score = score + (char) num;
		}
		System.out.println(score);

		// 3) 자료가공
		String[] arr = score.split("/");
		for (int i = 0; i < arr.length; i++) {
			System.out.println(arr[i]);
		}
		int[] kor = new int[10];
		int[] math = new int[10];
		for (int i = 0; i < arr.length; i++) {
			String[] t = arr[i].split(","); // t[0] -> "88", t[1] -> "60"
			kor[i] = Integer.parseInt(t[0]);
			math[i] = Integer.parseInt(t[1]);
		}
		for (int i = 0; i < kor.length; i++) {
			System.out.print(kor[i] + " ");
		}
		System.out.println("");
		for (int i = 0; i < math.length; i++) {
			System.out.print(math[i] + " ");
		}
		System.out.println("");
		// 4) 표현(계산,....)
		// 4-1) 국어 합계, 평균
		int totalKor = 0;
		double avgKor = 0;
		for (int i = 0; i < kor.length; i++) {
			totalKor = totalKor + kor[i];
		}
		avgKor = (double) totalKor / (double) kor.length;
		System.out.println("합계 : " + totalKor + ", 평균 : " + avgKor);
		// 4-2) 수학 합계, 평균

		// 4-3) 국어의 최대값, 최소값

		// 4-4) 수학의 최대값, 최소값
	}
}

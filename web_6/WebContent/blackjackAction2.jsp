<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ page import="vo.Card"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
</head>
<body>
	<%
	Card[] deck = new Card[52];
	String[] kinds = {"spade", "diamond", "heart", "clover"};

	for (int i = 0; i < kinds.length; i++) {
		for (int j = 0; j < 13; j++) {
			// 52장을 만들 것. → 4(0, 1, 2, 3) * j
			// 13장마다 종류가 다를 것.
			// 13장마다 숫자가 초기화. 1~13
			deck[(i * 13) + j] = new Card();
			deck[(i * 13) + j].kind = kinds[i];
			deck[(i * 13) + j].num = (j % 13) + 1;
		}
	}

	for (int i = 0; i < 10000; i++) {
		int r = (int) (Math.random() * deck.length);
		Card temp = deck[0];
		deck[0] = deck[r];
		deck[r] = temp;
	}

	for (int i = 0; i < 3; i++) {
	%>
	<img src="./image/<%=deck[i].kind + deck[i].num%>.jpg">
	<%
	}
	%>
</body>
</html>
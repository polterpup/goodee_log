<%@ page language="java" contentType="text/html; charset=EUC-KR"
	pageEncoding="EUC-KR"%>
<%@ page import="vo.Card"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="EUC-KR">
</head>
<body>
	<%
	// 1. 1~13 * 4 = 52장의 카드
	Card[] deck = new Card[52];

	/*
	String[] kinds = {"spade", "diamond", "heart", "clover"};

	for(String k : kinds) {
		for(int i=0; i<13; i++) {
			deck[i] = new Card();
		}
	}
	*/

	for (int i = 0; i < deck.length; i++) { // *52
		deck[i] = new Card(); // 0, null
		// 무늬 세팅
		if (i / 13 == 0) {
			deck[i].kind = "spade";
		} // i: 0~12
		else if (i / 13 == 1) {
			deck[i].kind = "diamond";
		} // i: 13~25
		else if (i / 13 == 2) {
			deck[i].kind = "heart";
		} // i: 26~38
		else if (i / 13 == 3) {
			deck[i].kind = "clover";
		} // i: 39~51
			// 번호 세팅
		deck[i].num = (i % 13) + 1;
	}

	/*	
	for (Card c : deck) {
		System.out.println(c.kind + " " + c.num);
	}
	*/

	for (int i = 0; i < 10000; i++) {
		int r = (int) (Math.random() * deck.length);
		Card temp = deck[0];
		deck[0] = deck[r];
		deck[r] = temp;
	}

	for (int i = 0; i < 3; i++) {
	%>
	<img src="./image/<%=deck[i].kind + deck[i].num%>.jpg">
	<%
	}
	%>
	<div></div>
</body>
</html>
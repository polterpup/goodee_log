<%@page import="jdk.internal.misc.FileSystemOption"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="java.io.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>
<%
// 인증 방어 코드 : 로그인 전에만 페이지 열람 가능
if (session.getAttribute("loginMember") != null) {
	System.out.println("[Error] : 이미 로그인 되어 있습니다.");
	response.sendRedirect("../index.jsp");
	return;
}

// 회원가입 입력 값들이 전부 입력됐는지 유효성 검사 필요X, required로 해결

PrintWriter write = response.getWriter();
request.setCharacterEncoding("utf-8");

/* System.out.println(request.getParameter("memberId"));
System.out.println(request.getParameter("memberPw"));
System.out.println(request.getParameter("memberName"));
System.out.println(request.getParameter("memberGender"));
System.out.println(request.getParameter("memberAge")); */

String memberId = request.getParameter("memberId");
String memberPw = request.getParameter("memberPw");
String memberName = request.getParameter("memberName");
String memberGender = request.getParameter("memberGender");
int memberAge = Integer.parseInt(request.getParameter("memberAge"));

// System.out.println(memberId + "\t" + memberPw + "\t" + memberName + "\t" + memberGender + "\t" + memberAge);

MemberDao memberDao = new MemberDao();
Member paramMember = new Member();

paramMember.setMemberId(memberId);
paramMember.setMemberPw(memberPw);
paramMember.setMemberName(memberName);
paramMember.setMemberGender(memberGender);
paramMember.setMemberAge(memberAge);

boolean ins_member = memberDao.insertMember(paramMember);

if (ins_member == true) {
	write.println("<script>alert('가입완료'); location.href='../index.jsp';</script>");
	write.flush();
} else {
	write.println("<script>alert('error!'); location.href='./insertMemberForm.jsp';</script>");
	write.flush();
}
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="vo.Board"%>
<%@ page import="dao.BoardDao"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link rel="stylesheet" href="./style.css">
</head>
<body>
	<div class="container-fluid">
		<%
		String boardCategory = "";
		if (request.getParameter("boardCategory") != null) {
			boardCategory = request.getParameter("boardCategory");
		}
		// boardCategory는 null or "csharp", "css", ....
		// null이면 전체에서 select
		// null아 아니면 boardCategory만 select

		BoardDao boardDao = new BoardDao();
		ArrayList<String> boardCategoryList = boardDao.selectBoardCategoryList();
		%>
		<!-- Header Start -->
		<div class="header">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
				<!-- Brand -->
				<a class="navbar-brand" href="./index.jsp">홈</a>

				<!-- Links -->
				<ul class="navbar-nav">
					<%
					if (session.getAttribute("loginMember") == null) {
					%>
					<li class="nav-item header-btns">
						<div class="btn-group">
							<button type="button" class="btn btn-warning dropdown-toggle"
								data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">로그인</button>
							<div class="dropdown-menu">
								<form method="POST" action="./Member/loginAction.jsp">
									<div>member Id :</div>
									<div>
										<input type="text" name="memberId">
									</div>
									<div>member Pw :</div>
									<div>
										<input type="password" name="memberPw">
									</div>
									<div>
										<button type="submit" class="btn btn-success btn-sm">로그인</button>
									</div>
								</form>
							</div>
						</div>
						<button type="button" class="btn btn-warning" onclick="location.href='./Member/insertMemberForm.jsp'">
							회원가입
						</button>
					</li>
					<%
					} else {
					%>
					<li class="nav-item"><a class="nav-link"
						href="./index.jsp?currentPage=1">전체게시판</a></li>

					<!-- Dropdown -->
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#" id="navbardrop"
						data-toggle="dropdown"> Category </a>
						<div class="dropdown-menu">
							<%
							for (String category : boardCategoryList) {
							%>
							<a class="dropdown-item"
								href="./index.jsp?currentPage=1&boardCategory=<%=category%>"><%=category%></a>
							<%
							}
							%>
						</div></li>
					<li class="nav-item header-btns">
						<button type="button" class="btn btn-warning"
							onclick="location.href='./Board/insertBoardForm.jsp'">글쓰기</button>
						<button type="button" class="btn btn-success logoutbtn"
							onclick="location.href='./Member/selectMemberOne.jsp'">
							<i class="bi bi-person-fill"></i>
						</button>
						<button type="button" class="btn btn-danger logoutbtn"
							onclick="location.href='./Member/logout.jsp'">
							<i class="bi bi-box-arrow-right"></i>
						</button>
					</li>
					<%
					}
					%>
				</ul>
			</nav>
		</div>
		<!-- Header End -->
		<!-- 본문내용 -->
		<%
		// 페이지번호 = currentPage를 1로 디폴트
		int currentPage = 1;
		// current가 null이 아니라면 값을 int 타입으로로 바꾸어서 페이지 번호로 씀
		if (request.getParameter("currentPage") != null) {
			currentPage = Integer.parseInt(request.getParameter("currentPage"));
		}
		// 화면에 보여질 페이지 갯수
		int BOARD_LIMIT = 15; // rowPerPage

		ArrayList<Board> boardList = boardDao.selectBoardListByPage(currentPage, BOARD_LIMIT, boardCategory);

		// currentPage만 숫자가 들어왔을떄. (전체게시판 출력)
		if (request.getParameter("currentPage") != null) {
		%>
		<div class="b_content">
			<table class="table table-striped table-dark table-hover">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">카테고리</th>
						<th scope="col">제목</th>
						<th scope="col">내용</th>
					</tr>
				</thead>
				<tbody>
					<%
					for (Board b : boardList) {
					%>
					<tr>
						<td><%=b.getBoardNo()%></td>
						<td><%=b.getBoardCategory()%></td>
						<td><a
							href="./Board/selectBoardOne.jsp?boardNo=<%=b.getBoardNo()%>&currentCommentPage=1"><%=b.getBoardTitle()%></a></td>
						<td><%=b.getBoardContent()%></td>
					</tr>
					<%
					}
					%>
				</tbody>
			</table>
		</div>
		<!-- 페이징 처리부분 -->
		<%
		// 한번에 보이는 페이징 수
		int PAGE_LIMIT = 10;

		int lastPage = boardDao.selectLastPage(BOARD_LIMIT);

		// 화면에 보여질 시작 페이지 번호
		// ((현재페이지번호 - 1) / 화면에 보여질 페이지 번호) * 화면에 보여질 페이지 번호 + 1
		// (currentPage - 1)을 하는 이유는 현재페이지가 10일시에도 startPage가 1이기 위해서
		int startPage = ((currentPage - 1) / PAGE_LIMIT) * PAGE_LIMIT + 1;

		// 화면에 보여질 마지막 페이지 번호
		int endPage = startPage + PAGE_LIMIT - 1;

		if (lastPage < endPage) {
			endPage = lastPage;
		}
		%>
		<div class="bg-dark text-white buttonbar">
			<%
			if (1 != startPage) {
			%>
			<button type="button"
				onclick="location.href='./index.jsp?currentPage=<%=startPage - PAGE_LIMIT%>&boardCategory=<%=boardCategory%>'">
				<i class="bi bi-chevron-double-left"></i>
			</button>
			<%
			}

			for (int i = startPage; i <= endPage; i++) {
			if (i == currentPage) {
			%>
			<a class="c_page"
				href="./index.jsp?currentPage=<%=i%>&boardCategory=<%=boardCategory%>"><%=i%></a>
			<%
			} else {
			%>
			<a class="page"
				href="./index.jsp?currentPage=<%=i%>&boardCategory=<%=boardCategory%>"><%=i%></a>
			<%
			}
			}

			if (endPage != lastPage) {
			%>
			<button type="button"
				onclick="location.href='./index.jsp?currentPage=<%=startPage + PAGE_LIMIT%>&boardCategory=<%=boardCategory%>' ">
				<i class="bi bi-chevron-double-right"></i>
			</button>
			<%
			}
			%>
		</div>
		<%
		}
		// if End
		%>
		<!-- 전체게시판 End -->

		<!-- jQuery library -->
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<!-- Popper JS -->
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
		<!-- Latest compiled JavaScript -->
		<script
			src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="dao.CommentDao"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	// 알람창 PrintWriter 클래스 사용 [ IO Package ]
	PrintWriter write = response.getWriter();
	request.setCharacterEncoding("utf-8");

	if (request.getParameter("boardNo") == null) {
		response.sendRedirect("../index.jsp");
		return;
	}

	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	String commentContent = request.getParameter("commentContent");
	
	// System.out.println(boardNo + " [boardNo]");
	
	CommentDao commentDao = new CommentDao();
	boolean ins_comment = commentDao.insertComment(boardNo, commentContent);
	
	
	if (ins_comment == true) {
		write.println("<script>alert('등록 되었습니다'); location.href='./selectBoardOne.jsp?boardNo=" + boardNo + "&currentCommentPage=1';</script>");
		write.flush();
	} else {
		write.println("<script>alert('error!'); location.href='./selectBoardOne.jsp?boardNo=" + boardNo + "&currentCommentPage=1';</script>");
		write.flush();
	}
	%>
</body>
</html>
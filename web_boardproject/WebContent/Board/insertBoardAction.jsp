<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*" %>
<%@ page import = "java.util.*" %>
<%@ page import="java.io.*"%>
<%@ page import = "vo.Board" %>
<%@ page import="dao.BoardDao"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	PrintWriter write = response.getWriter();
	request.setCharacterEncoding("utf-8");

	if(request.getParameter("boardTitle")==""){
	   response.sendRedirect("../index.jsp?currentPage=1");
	   return;
	}
	
	String boardCategory = request.getParameter("boardCategory");
	String boardTitle = request.getParameter("boardTitle");
	String boardContent = request.getParameter("boardContent");
	
	BoardDao boardDao = new BoardDao();
	boolean ins_board = boardDao.insertBoard(boardCategory, boardTitle, boardContent);
	
	if (ins_board == true) {
		write.println("<script>alert('등록 되었습니다'); location.href='../index.jsp?currentPage=1';</script>");
		write.flush();
	} else {
		write.println("<script>alert('error!'); location.href='../index.jsp?currentPage=1';</script>");
		write.flush();
	}
%>
</body>
</html>
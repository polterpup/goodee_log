<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="vo.*"%>
<%@ page import="dao.*"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link rel="stylesheet" href="../style.css">
</head>
<body>
	<%
	request.setCharacterEncoding("utf-8");

	//	방어코드
	if (request.getParameter("boardNo") == null) {
		response.sendRedirect("./selectBoardList.jsp?currentPage=1");
		return;
	}

	// selectBoardList에서 넘겨받은 boardNo 저장
	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	BoardDao boardDao = new BoardDao();
	Board board = boardDao.selectBoardOne(boardNo);
	%>
	<div class="container-fluid">
		<!-- Header Start -->
		<div class="header">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
				<!-- Brand -->
				<a class="navbar-brand" href="../index.jsp">홈</a>
				<!-- Links -->
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link"
						href="../index.jsp?currentPage=1">전체게시판 </a></li>
				</ul>
			</nav>
		</div>
		<!-- Header End -->
		<!-- 본문내용 -->
		<div class="jumbotron">
			<h1>
				게시글 #<%=boardNo%>
				수정
			</h1>
		</div>
		<form action="./updateBoardAction.jsp" class="insertBF">
			<input type="hidden" name="boardNo" value="<%=boardNo%>">
			<div class="input-group mb-2">
				<div class="input-group-prepend">
					<select class="custom-select" name="boardCategory">
						<option selected><%=board.getBoardCategory()%></option>
						<option value="Csharp">Csharp</option>
						<option value="CSS">CSS</option>
						<option value="HTML">HTML</option>
						<option value="JAVA">JAVA</option>
						<option value="JavaScript">JavaScript</option>
						<option value="JQuery">JQuery</option>
						<option value="MyBatis">MyBatis</option>
						<option value="Python">Python</option>
						<option value="Spring">Spring</option>
						<option value="SQL">SQL</option>
					</select>
				</div>
				<input type="text" class="form-control" name="boardTitle"
					value="<%=board.getBoardTitle()%>">
			</div>
			<textarea class="form-control" name="boardContent" rows="15"><%=board.getBoardContent()%></textarea>
			<button type="submit" class="btn btn-success center">등록</button>
		</form>
	</div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="dao.BoardDao"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	/*
	update board set board_category=?, board_title=?, board_content=? where board_no=?
	*/
	
	PrintWriter write = response.getWriter();
	
	request.setCharacterEncoding("utf-8");

	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	String boardCategory = request.getParameter("boardCategory");
	String boardTitle = request.getParameter("boardTitle");
	String boardContent = request.getParameter("boardContent");
	
	BoardDao boardDao = new BoardDao();
	boolean updateBoard = boardDao.updateBoard(boardNo, boardCategory, boardTitle, boardContent);
	
	if (updateBoard == true) {
		write.println("<script>alert('수정 완료'); location.href='./selectBoardOne.jsp?boardNo=" + boardNo + "&currentCommentPage=1';</script>");
		write.flush();
	} else {
		write.println("<script>alert('error!'); location.href='./selectBoardOne.jsp?boardNo=" + boardNo + "&currentCommentPage=1';</script>");
		write.flush();
	}
	%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="vo.Board"%>
<%@ page import="vo.Comment"%>
<%@ page import="dao.BoardDao"%>
<%@ page import="dao.CommentDao"%>
<%@ page import="db.DBUtill"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link rel="stylesheet" href="../style.css">
</head>
<body>
	<div class="container-fluid">
		<%
		request.setCharacterEncoding("utf-8");

		int boardNo = Integer.parseInt(request.getParameter("boardNo"));

		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection(); // this.getConnection(); 에서 this 생략
		%>
		<!-- Header Start -->
		<div class="header">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
				<!-- Brand -->
				<a class="navbar-brand" href="../index.jsp">홈</a>
				<!-- Links -->
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link"
							href="../index.jsp?currentPage=1">전체게시판
						</a>
					</li>
				</ul>
			</nav>
		</div>
		<!-- Header End -->
		<!-- 본문내용 -->
		<div class="jumbotron">
			<h1>게시글 상세보기</h1>
		</div>
		<%
		BoardDao boardDao = new BoardDao();
		Board board = boardDao.selectBoardOne(boardNo);
		%>
		<div>
			<table class="table table-dark">
				<tr>
					<td>board_no</td>
					<td><%=boardNo%></td>
				</tr>
				<tr>
					<td>board_category</td>
					<td><%=board.getBoardCategory()%></td>
				</tr>
				<tr>
					<td>board_title</td>
					<td><%=board.getBoardTitle()%></td>
				</tr>
				<tr>
					<td>board_content</td>
					<td><%=board.getBoardContent()%></td>
				</tr>
				<tr>
					<td>board_date</td>
					<td><%=board.getBoardDate()%></td>
				</tr>
				<tr>
					<td colspan="2" class="btns">
					<a href="./deleteBoardAction.jsp?boardNo=<%=boardNo%>" class="btn btn-danger" onclick="if(!confirm('삭제 하시겠습니까?')){return false;}">삭제</a>
					<a href="./updateBoardForm.jsp?boardNo=<%=boardNo%>" class="btn btn-success">수정</a>
					</td>
				</tr>
			</table>
		</div>
		<!-- Table End -->
		<h2 class="commentTitle">댓글입력</h2>
		<div class="inputComment">
			<form action="./insertCommentAction.jsp" method="post">
				<input type="hidden" name="boardNo" value="<%=boardNo%>">
				<div class="form-group">
					<label for="comment">Comment:</label>
					<textarea name="commentContent" class="form-control" rows="5"></textarea>
				</div>
				<button class="btn btn-warning" type="submit">댓글입력</button>
			</form>
		</div>
		<hr>
		<h2 class="commentTitle">댓글목록</h2>
		<!-- 댓글목록 출력, 10개씩 페이징 -->
			<%
			
			
			// 화면에 보여질 댓글 갯수
			int COMMENT_LIMIT = 10;
			
			int currentCommentPage = 1;
			// currentCommentPage가 null이 아니라면 값을 int 타입으로로 바꾸어서 페이지 번호로 씀
			if (request.getParameter("currentCommentPage") != null) {
				currentCommentPage = Integer.parseInt(request.getParameter("currentCommentPage"));
			}
			
			CommentDao commentDao = new CommentDao();
			ArrayList<Comment> commentList = commentDao.commentListByPage(boardNo, currentCommentPage, COMMENT_LIMIT);			
			
			for (Comment c : commentList) {%>
				<div class="commentbox">
					<span>
						<small><%=c.getCommentDate()%></small>
					</span>
					<br>
					<p><%=c.getCommentContent()%></p>
					<a href="./deleteComment.jsp?commentNo=<%=c.getCommentNo()%>&boardNo=<%=boardNo%>"  class="btn btn-danger" onclick="if(!confirm('댓글을 삭제 하시겠습니까?')){return false;}">삭제</a>
					<!-- commentNo의 덧글을 삭제 후 다시 이 페이지 되돌아오기(redirect) 위해서 boardNo 넘겨준다 -->
				</div>
			<%}
			
			
			// 한번에 보이는 페이징 수
			int PAGE_LIMIT = 10;
			
			int lastPage = commentDao.commentLastPage(COMMENT_LIMIT, boardNo);
			
			
			// 화면에 보여질 시작 페이지 번호
			int startPage = ((currentCommentPage - 1) / PAGE_LIMIT) * PAGE_LIMIT + 1;

			// 화면에 보여질 마지막 페이지 번호
			int endPage = startPage + PAGE_LIMIT - 1;
			
			if (lastPage < endPage) {
				endPage = lastPage;
			}
			%>
			<div class="bg-dark text-white buttonbar">
			<%
			if (1 != startPage) {
			%>
			<button type="button"
				onclick="location.href='./selectBoardOne.jsp?boardNo=<%=boardNo%>&currentCommentPage=<%=startPage - PAGE_LIMIT%>'">
				<i class="bi bi-chevron-double-left"></i>
			</button>
			<%
			}

			for (int i = startPage; i <= endPage; i++) {
			if (i == currentCommentPage) {
			%>
			<a class="c_page"
				href="./selectBoardOne.jsp?boardNo=<%=boardNo%>&currentCommentPage=<%=i%>"><%=i%></a>
			<%
			} else {
			%>
			<a class="page"
				href="./selectBoardOne.jsp?boardNo=<%=boardNo%>&currentCommentPage=<%=i%>"><%=i%></a>
			<%
			}
			}

			if (endPage != lastPage) {
			%>
			<button type="button"
				onclick="location.href='./selectBoardOne.jsp?boardNo=<%=boardNo%>&currentCommentPage=<%=startPage + PAGE_LIMIT%>'">
				<i class="bi bi-chevron-double-right"></i>
			</button>
			<%
			}
			%>
		</div>
		<!-- Comment End -->
	</div>
</body>
</html>
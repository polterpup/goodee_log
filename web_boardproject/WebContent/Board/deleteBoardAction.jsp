<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="dao.BoardDao"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	// 알람창 PrintWriter 클래스 사용 [ IO Package ]
	PrintWriter write = response.getWriter();
	
	request.setCharacterEncoding("utf-8");

	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	
	// Debugging..
	/*
	System.out.println(boardNo);
	*/
	
	BoardDao boardDao = new BoardDao();
	boolean del_board = boardDao.deleteBoard(boardNo);
	
	if (del_board == true) {
		write.println("<script>alert('삭제 되었습니다'); location.href='../index.jsp?currentPage=1';</script>");
		write.flush();
	} else {
		write.println("<script>alert('error!'); location.href='../index.jsp?currentPage=1';</script>");
		write.flush();
	}
	%>
</body>
</html>
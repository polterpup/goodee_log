<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.io.*"%>
<%@ page import="dao.CommentDao"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	// 알람창 PrintWriter 클래스 사용 [ IO Package ]
	PrintWriter write = response.getWriter();
		
	request.setCharacterEncoding("utf-8");

	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	int commentNo = Integer.parseInt(request.getParameter("commentNo"));
	
	CommentDao commentDao = new CommentDao();
	boolean del_comment = commentDao.deleteComment(boardNo, commentNo);
	
	if (del_comment == true) {
		write.println("<script>alert('삭제 되었습니다'); location.href='./selectBoardOne.jsp?boardNo=" + boardNo + "&currentCommentPage=1';</script>");
		write.flush();
	} else {
		write.println("<script>alert('error!'); location.href='./selectBoardOne.jsp?boardNo=" + boardNo + "&currentCommentPage=1';</script>");
		write.flush();
	}
	%>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
<link rel="stylesheet" href="../style.css">
</head>
<body>
	<div class="container-fluid">
		<%
		request.setCharacterEncoding("utf-8");
		%>
		<!-- Header Start -->
		<div class="header">
			<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
				<!-- Brand -->
				<a class="navbar-brand" href="../index.jsp">홈</a>
				<!-- Links -->
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link"
						href="../index.jsp?currentPage=1">전체게시판 </a></li>
				</ul>
			</nav>
		</div>
		<!-- Header End -->
		<!-- 본문내용 -->
		<div class="jumbotron">
			<h1>게시글 입력</h1>
		</div>
		<form action="./insertBoardAction.jsp" class="insertBF">
			<div class="input-group mb-2">
			  <div class="input-group-prepend">
			  	<select class="custom-select" name="boardCategory">
				  <option selected>Category</option>
				  <option value="Csharp">Csharp</option>
				  <option value="CSS">CSS</option>
				  <option value="HTML">HTML</option>
				  <option value="JAVA">JAVA</option>
				  <option value="JavaScript">JavaScript</option>
				  <option value="JQuery">JQuery</option>
				  <option value="MyBatis">MyBatis</option>
				  <option value="Python">Python</option>
				  <option value="Spring">Spring</option>
				  <option value="SQL">SQL</option>
				</select>
			  </div>
			  <input type="text" class="form-control" name="boardTitle" placeholder="제목을 입력해주세요">
			</div>
			<textarea class="form-control" name="boardContent" rows="15"></textarea>
			<button type="submit" class="btn btn-success center">등록</button>
		</form>
	</div>
</body>
</html>
package dao;

import java.sql.*;

import db.DBUtil;
import vo.Member;

public class MemberDao {
	// 1. 회원탈퇴
	public int deleteMember(Member member) {
		// 매개변수 값은 무조건 디버깅
		System.out.println(member.getMemberId() + " [MemberDao.deleteMember param : memberId]");
		System.out.println(member.getMemberPw() + " [MemberDao.deleteMember param : memberPw]");

		return 0; // 수정하기
	}

	// 2. 회원수정
	// 2-1. 비밀번호 수정
	public int updateMemberPw(Member member, String memberPwNew) {
		// 매개변수 값은 무조건 디버깅

		return 0; // 수정하기
	}

	// 2-2. 기타 수정

	// 3. 회원정보 출력
	public Member selectMemberOne(String memberId) {
		// 매개변수 값은 무조건 디버깅
		
		return null; // 수정하기
	}

	// 4. 회원가입
	public boolean insertMember(Member member) throws ClassNotFoundException, SQLException {
		// 매개변수 값은 무조건 디버깅
//		System.out.println(member.getMemberId() + " [MemberDao.insertMember param : memberId]");
//		System.out.println(member.getMemberPw() + " [MemberDao.insertMember param : memberPw]");
//		System.out.println(member.getMemberName() + " [MemberDao.insertMember param : memberName]");
//		System.out.println(member.getMemberGender() + " [MemberDao.insertMember param : memberGender]");
//		System.out.println(member.getMemberAge() + " [MemberDao.insertMember param : memberAge]");

		// DB
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		// mysql은 8.x 버전부터 PASSWORD함수를 지원하지 않기 때문에 SHA2(혹은 SHA1, md5와 같은)함수를 사용. SHA2의 경우 SHA2(Pw, 길이)와 같이 작성
		String sql = "INSERT INTO member(member_id, member_pw, member_name, member_age, member_gender, member_date) VALUES (?, SHA2(?, 256), ?, ?, ?, NOW())";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, member.getMemberId());
		stmt.setString(2, member.getMemberPw());
		stmt.setString(3, member.getMemberName());
		stmt.setInt(4, member.getMemberAge());
		stmt.setString(5, member.getMemberGender());
		

		int success = stmt.executeUpdate();

		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	// 5. 로그인 : 성공시 Member(memberId, memberPw) 리턴 | 실패시 null 리턴
	public Member login(Member member) throws ClassNotFoundException, SQLException {

		// 매개변수 값은 무조건 디버깅
		/*
		 * System.out.println(member.getMemberId() +
		 * " [MemberDao.deleteMember param : memberId]");
		 * System.out.println(member.getMemberPw() +
		 * " [MemberDao.deleteMember param : memberPw]");
		 */

		// DB
		DBUtil dbUtil = new DBUtil();
		Connection conn = dbUtil.getConnection();
		// mysql은 8.x 버전부터 PASSWORD함수를 지원하지 않기 때문에 SHA2(혹은 SHA1, md5와 같은)함수를 사용.
		String sql = "SELECT member_id AS memberId, member_pw AS memberPw, member_name AS memberName FROM member WHERE member_id=? AND member_pw=SHA2(?, 256)";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, member.getMemberId());
		stmt.setString(2, member.getMemberPw());

		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {
			Member returnMember = new Member();
			returnMember.setMemberId(rs.getString("memberId"));
			returnMember.setMemberName(rs.getString("memberName"));
			return returnMember;
		}

		return null;
	}
}

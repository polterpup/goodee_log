package dao;

import java.sql.*;
import java.util.*;

import db.DBUtil;
import vo.Board;
import vo.Comment;

public class CommentDao {
	public boolean deleteComment(int boardNo, int commentNo) throws ClassNotFoundException, SQLException {
		// DB
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection(); // this.getConnection(); 에서 this 생략

		PreparedStatement stmt = conn.prepareStatement("DELETE FROM comment WHERE board_no=? AND comment_no=?");
		stmt.setInt(1, boardNo);
		stmt.setInt(2, commentNo);
		
		int success = stmt.executeUpdate();
		
		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	
	public boolean insertComment(int boardNo, String commentContent) throws ClassNotFoundException, SQLException {
		// DB
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection(); // this.getConnection(); 에서 this 생략

		String sql = "INSERT INTO comment(board_no, comment_content, comment_date) VALUES (?, ?, now())";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, boardNo);
		stmt.setString(2, commentContent);

		int success = stmt.executeUpdate();
		
		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	public int commentLastPage(int COMMENT_LIMIT, int boardNo) throws ClassNotFoundException, SQLException {
		// 댓글 페이징
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection(); // this.getConnection(); 에서 this 생략
		String sql = "SELECT count(*) FROM comment WHERE board_no=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, boardNo);

		int totalComment = 0;

		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			totalComment = rs.getInt("count(*)");
		}

		int lastPage = totalComment / COMMENT_LIMIT;

		if (totalComment % COMMENT_LIMIT != 0) {
			lastPage += 1;
		}

		return lastPage;
	}

	public ArrayList<Comment> commentListByPage(int boardNo, int currentCommentPage, int COMMENT_LIMIT)
			throws ClassNotFoundException, SQLException {
		// 총 댓글 수 [현재 게시]
		int totalComment = 0;

		// 페이지 시작 값
		int beginRow = (currentCommentPage - 1) * COMMENT_LIMIT;

		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection(); // this.getConnection(); 에서 this 생략
		String sql = "SELECT * FROM comment WHERE board_no=? ORDER BY comment_date DESC LIMIT ?, ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, boardNo);
		stmt.setInt(2, beginRow);
		stmt.setInt(3, COMMENT_LIMIT);

		ResultSet rs = stmt.executeQuery();
		ArrayList<Comment> commentList = new ArrayList<>();

		while (rs.next()) {
			Comment c = new Comment();
			c.setCommentNo(rs.getInt("comment_no"));
			c.setCommentContent(rs.getString("comment_content"));
			c.setCommentDate(rs.getString("comment_date"));
			commentList.add(c);
		}

		return commentList;
	}
}

package dao;

import java.sql.*;
import java.util.*;

import db.DBUtil;
import vo.Board;

public class BoardDao {
	public boolean deleteBoard(int boardNo)
			throws ClassNotFoundException, SQLException {
		// DB
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection(); // this.getConnection(); 에서 this 생략
		
		PreparedStatement stmt = conn.prepareStatement("DELETE FROM board WHERE board_no=?");
		stmt.setInt(1, boardNo);

		int success = stmt.executeUpdate();
		
		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean insertBoard(String boardCategory, String boardTitle, String boardContent)
			throws ClassNotFoundException, SQLException {
		Board board = new Board();
		board.setBoardCategory(boardCategory);
		board.setBoardTitle(boardTitle);
		board.setBoardContent(boardContent);

		// DB
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();

		// Query [insert]
		String sql = "INSERT INTO board(board_category, board_title, board_content,board_date) VALUES(?,?,?,now())";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, board.getBoardCategory());
		stmt.setString(2, board.getBoardTitle());
		stmt.setString(3, board.getBoardContent());

		int success = stmt.executeUpdate();

		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean updateBoard(int boardNo, String boardCategory, String boardTitle, String boardContent)
			throws ClassNotFoundException, SQLException {
		// DB
		DBUtil dbutil = new DBUtil();
		Connection conn = dbutil.getConnection();

		PreparedStatement stmt = conn
				.prepareStatement("UPDATE board SET board_category=?, board_title=?, board_content=? WHERE board_no=?");

		stmt.setString(1, boardCategory);
		stmt.setString(2, boardTitle);
		stmt.setString(3, boardContent);
		stmt.setInt(4, boardNo);

		int success = stmt.executeUpdate();

		System.out.println("");

		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	public Board selectBoardOne(int boardNo) throws ClassNotFoundException, SQLException {
		Board board = null;
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		String sql = "SELECT * FROM board WHERE board_no=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, boardNo);

		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			board = new Board();
			board.setBoardCategory(rs.getString("board_category"));
			board.setBoardTitle(rs.getString("board_title"));
			board.setBoardContent(rs.getString("board_content"));
			board.setBoardDate(rs.getString("board_date"));
		}

		return board;
	}

	public int selectLastPage(int BOARD_LIMIT) throws ClassNotFoundException, SQLException {
		int lastPage = 0;
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		String sql = "SELECT COUNT(*) from board";
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		int totalPost = 0;
		if (rs.next()) {
			totalPost = rs.getInt("COUNT(*)");
		}
		lastPage = totalPost / BOARD_LIMIT;
		if (totalPost % BOARD_LIMIT != 0) {
			lastPage += 1;
		}
		return lastPage;
	}

	public ArrayList<Board> selectBoardListByPage(int currentPage, int BOARD_LIMIT, String boardCategory)
			throws ClassNotFoundException, SQLException {

		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();

		// 총 게시물 수
		int totalPost = 0;
		// 페이지 시작 값
		int beginRow = (currentPage - 1) * BOARD_LIMIT;

		String sql = null;
		PreparedStatement stmt = null;

		if (boardCategory.equals("")) {
			sql = "SELECT * FROM board ORDER BY board_no DESC limit ?,?";
			stmt = conn.prepareStatement(sql);
			stmt.setInt(1, beginRow);
			stmt.setInt(2, BOARD_LIMIT);
		} else {
			sql = "SELECT * FROM board WHERE board_category=? ORDER BY board_no DESC limit ?,?";
			stmt = conn.prepareStatement(sql);
			stmt.setString(1, boardCategory);
			stmt.setInt(2, beginRow);
			stmt.setInt(3, BOARD_LIMIT);
		}

		ResultSet rs = stmt.executeQuery();
		ArrayList<Board> boardList = new ArrayList<>();

		while (rs.next()) {
			Board board = new Board();
			board.setBoardNo(rs.getInt("board_no"));
			board.setBoardCategory(rs.getString("board_category"));
			board.setBoardTitle(rs.getString("board_title"));
			board.setBoardContent(rs.getString("board_content"));
			boardList.add(board);
		}

		return boardList;
	}

	public ArrayList<String> selectBoardCategoryList() throws ClassNotFoundException, SQLException {
		ArrayList<String> boardCategoryList = new ArrayList<>();

		// DB 연결
		DBUtil dbutil = new DBUtil();

		Connection conn = dbutil.getConnection();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT DISTINCT board_category FROM board ORDER BY board_category ASC");
		// distinct 통해서 category별 하나
		ResultSet rs = stmt.executeQuery();

		while (rs.next()) {
			boardCategoryList.add(rs.getString("board_category"));
		}
		return boardCategoryList;
	}
}

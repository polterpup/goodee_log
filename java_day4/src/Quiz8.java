import java.util.*;

public class Quiz8 {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);

		System.out.print("숫자 입력 : ");
		int num = s.nextInt();
		int count = 0;
		
		for (int i=2; i<num; i++) {
			if (num % i == 0) {
				count ++;
			}
		}
		
		if (count == 0) {
			System.out.println(num + "은 소수입니다.");
		} else {
			System.out.println(num + "은 소수가 아닙니다.");
		}
	}

}

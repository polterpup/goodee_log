<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h1>학생정보 입력</h1>
	<form action="./insertStudentAction.jsp" style="width: 245px">
		<table border="1">
			<tr>
				<th>학생 이름</th>
				<td><input type="text" name="id"></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td><input type="password" name="pw"></td>
			</tr>
		</table>
		<button type="submit" style="margin: 10px 0 0 10px; float: right;">입력</button>
	</form>
</body>
</html>
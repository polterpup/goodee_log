<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="vo.Card"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
	int count = 2; // 기본 값 2로 설정
	if (request.getParameter("count") != null) { // 값이 들어오면 들어온 값으로 수정
		count = Integer.parseInt(request.getParameter("count"));
	}

	Card[] deck = new Card[52];
	String[] kinds = { "spade", "diamond", "heart", "clover" };

	for (int i = 0; i < kinds.length; i++) { // *4
		for (int j = 0; j < 13; j++) { // *13
			deck[(i * 13) + j] = new Card();
			deck[(i * 13) + j].kind = kinds[i];
			deck[(i * 13) + j].num = j + 1;
		}
	}

	for (int i = 0; i < 10000; i++) {
		int r = (int) (Math.random() * deck.length);// 0~51
		Card temp = deck[0];
		deck[0] = deck[r];
		deck[r] = temp;
	}

	// 게임규칙, 점수계산
	// 1) 카드3장 숫자의 합이 21을 넘지 않으면서 21에 가까운 사람이 승
	// 2) J(10), Q(10), K(10), 나머지카드 (숫자)
	// 3) 과제 : A는 11인데 합이 21을 넘으면

	int[] playerSum = new int[count];

	for (int i = 0; i < 3 * count; i++) {
		// for (int i = 0; i < 3; i++) {
		if (i % 3 == 0) {
	%>
	<br>
	<%
	}
	%>
	<img src="./image/<%=deck[i].kind + deck[i].num%>.jpg">
	<%
	// i : 0, 1, 2 --> playerSum[0]
	// i : 3, 4, 5 --> playerSum[1]
	// i : 6, 7, 8 --> playerSum[2]

	int opNum = deck[i].num;
	// deck[i].num J, Q, K면
	if (deck[i].num == 11 || deck[i].num == 12 || deck[i].num == 13) {
		opNum = 10;
	}
	playerSum[i / 3] += opNum;
	}

	for (int i = 0; i < playerSum.length; i++) {
	%>
	<div><%=i + 1%>번째 플레이어 카드 합 :
		<%=playerSum[i]%></div>
	<%
	}
	%>
</body>
</html>
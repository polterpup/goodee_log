<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.DriverManager"%> <!-- DB접속 -->
<%@ page import="java.sql.Connection"%> <!-- SQL사용 -->
<%@ page import="java.sql.PreparedStatement"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	String id = request.getParameter("id");
	String pw = request.getParameter("pw");

	// 1) MariaDB (Mysql)를 사용하기 위해서....
	Class.forName("org.mariadb.jdbc.Driver"); // 입력값 : 클래스의 풀네임을 문자열로
	//System.out.println("mariadb 사용가능");
	
	// 2) MariaDB 접속
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/onepiece", "root", "java1004");
	System.out.println(conn);
	
	// 3) Connection에서 쿼리 실행자를 호출
	PreparedStatement stmt = conn.prepareStatement("insert into student(id, pw) values(?, ?)"); // ? 표현식 : 변수자리
	
	// 3-2) 쿼리 표현식 완		
	stmt.setString(1, id);
	stmt.setString(2, pw); // DB에 들어가는 순으로 입력
	
	// 4) 쿼리 실행
	stmt.executeUpdate();
	%>
</body>
</html>
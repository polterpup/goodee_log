import java.util.*;
import java.sql.*;
import view.Menu;
import dao.MemberDao;
import vo.Member;

public class App {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		while (true) {
			// 메뉴를 직접 출력 -> 메뉴를 출력하는 메서드를 호출
			Menu menu = new Menu();
			menu.printMainMenu();

			Scanner scanner = new Scanner(System.in);

			int menuNum = 0;
			menuNum = scanner.nextInt();

			if (menuNum == 1) { // 멤버 조회
				menu.printSelectSubMenu(); // 입력값X, 리턴값X
				int submenuNum = 0;
				submenuNum = scanner.nextInt();

				if (submenuNum == 1) { // 1-1 전체 멤버
					
					int currentPage = 1;
					
					// 전체 멤버 호출 메서드
					MemberDao memberDao = new MemberDao();
					ArrayList<Member> list = memberDao.selectMemberList(currentPage);
					
					for(Member m : list) {
						System.out.print(m.memberId + "\t\t");
						System.out.print(m.memberPw + "\t\t");
						System.out.println(m.memberName);
					}
					boolean inLoof = true;
					
					while(inLoof) {
						System.out.println("이전(<) 다음(>)을 선택하세요");
						String preOrNext = null;
						preOrNext = scanner.next();
						
						if(preOrNext.equals("<")) { // 이전
							// selectMemberList 호출
							System.out.println("이전 선택 .......");
							currentPage = currentPage - 1;
							if(currentPage == 0) {
								System.out.println("더 이상 이전 페이지를 불러올 수 없습니다");
								currentPage = 1;
							}
							System.out.println(currentPage+"번쨰 페이지");
							ArrayList<Member> prelist = memberDao.selectMemberList(currentPage); // ResultSet 결과물 리턴
							for(Member m : prelist) {
								System.out.print(m.memberId+"\t");
								System.out.print(m.memberPw+"\t");
								System.out.print(m.memberName+"\n");
							}
							
						} else if(preOrNext.equals(">")) { // 다음
							// selectMemberList 호출
							System.out.println("다음 선택 .......");
							currentPage = currentPage + 1;
							System.out.println(currentPage+"번쨰 페이지");
							ArrayList<Member> nextlist = memberDao.selectMemberList(currentPage); // ResultSet 결과물 리턴
							for(Member m : nextlist) {
								System.out.print(m.memberId+"\t");
								System.out.print(m.memberPw+"\t");
								System.out.print(m.memberName+"\n");
							}
						} else if(preOrNext.equals("q")) { // 종료
							// while 탈출 (레벨링)
							inLoof = false;
						} else {
							System.out.println("페이징 메뉴를 잘못 선택했습니다.");
						}
					}
					// 전체 멤버 조회 End
				} else if (submenuNum == 2) { // 1-2 특정 멤버
					System.out.println("\t조회할 멤버의 ID를 입력하세요 : ");
					String memberId = null;
					memberId = scanner.next();
					
					MemberDao memberDao = new MemberDao();
					Member member = null;
	                member = memberDao.selectMemberOne(memberId);
	            	
	            	if (member.memberId == null) {
	            		System.out.println(memberId + "라는 member_id가 존재하지 않습니다.");
	            	} else {
	            		System.out.println(member.memberId + "\t\t" + member.memberPw + "\t\t" + member.memberName);
	            	}
					// 특정멤버 조회 End
				} else {
					System.out.println("조회 서브메뉴는 1~2번 중에서 선택하셔야 합니다.");
				}
				
				System.out.println("");
				// 멤버 조회 End
			} else if (menuNum == 2) { // 멤버 추가
				String memberId = null;
				String memberPw = null;
				String memberName = null;

				System.out.println("추가할 멤버의 ID를 입력하세요 : ");
				memberId = scanner.next();

				System.out.println("추가할 멤버의 Pw를 입력하세요 : ");
				memberPw = scanner.next();

				System.out.println("추가할 멤버의 Name를 입력하세요 : ");
				memberName = scanner.next();
				
				Member member = new Member();
				member.memberId = memberId;
				member.memberPw = memberPw;
				member.memberName = memberName;
				
				MemberDao memberDao = new MemberDao();
				boolean success = memberDao.insertMember(member);
				
				if (success == true) {
					System.out.println("추가 성공");
				} else {
					System.out.println("추가 실패");
				}
				
				// 멤버 추가 End
			} else if (menuNum == 3) {
				String memberId = null;
				String memberPw = null; // Original Password
				String memberPwNew = null; // New Password

				System.out.println("수정할 멤버 ID를 입력하세요 : ");
				memberId = scanner.next();

				System.out.println("수정할 멤버의 기존 Pw를 입력하세요 : ");
				memberPw = scanner.next();

				System.out.println("수정할 멤버의 변경할 Pw를 입력하세요 : ");
				memberPwNew = scanner.next();
				
				MemberDao memberDao = new MemberDao();
				boolean success = memberDao.updateMember(memberId, memberPw, memberPwNew);
				
				if (success == true) {
					System.out.println("수정 성공\n");
				} else {
					System.out.println("수정 실패\n");
				}
				
				// 멤버 수정 End
			} else if (menuNum == 4) {
				String memberId = null;
				String memberPw = null;

				System.out.println("삭제할 멤버 ID를 입력하세요 : ");
				memberId = scanner.next();

				System.out.println("삭제할 멤버 Pw를 입력하세요 : ");
				memberPw = scanner.next();

				MemberDao memberDao = new MemberDao();
				Member member = new Member();
				member.memberId = memberId;
				member.memberPw = memberPw;
				
				boolean success = memberDao.deleteMember(member);
				
				if (success == true) {
					System.out.println("ID : " + memberId + " 멤버 삭제 성공\n");
				} else {
					System.out.println("삭제 실패\n");
				}
				
				// 멤버 삭제 End
			} else if (menuNum == 5) {
				System.out.println("종료 합니다.");
				break;
			} else {
				System.out.println("잘못 선택하셨습니다. 1~5번 중에 선택해주세요.");
				System.out.println("");
			}
		}
	}
}

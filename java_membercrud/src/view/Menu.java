package view;
public class Menu {
	public void printMainMenu() {
		System.out.println("멤버 관리 메뉴를 선택하세요. (1번~5번) : ");
		System.out.println("1. 멤버 조회");
		System.out.println("2. 멤버 추가");
		System.out.println("3. 멤버 수정");
		System.out.println("4. 멤버 삭제");
		System.out.println("5. 종료");
	}
	
	public void printSelectSubMenu() {
		System.out.println("\t1. 멤버 조회 서브메뉴를 선택하세요. (1번~2번) : ");
		System.out.println("\t1-1. 전체 멤버 조회");
		System.out.println("\t1-2. 특정 멤버 조회");
	}
}

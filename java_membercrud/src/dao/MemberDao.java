package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import vo.Member;

// member 테이블을 접근해서 sql명령을 실행시키는 메서드들을 모아둘 클래스	
public class MemberDao {

	public Member selectMemberOne(String memberId) throws ClassNotFoundException, SQLException {
		Member member = null;

		// DB
		Class.forName("org.mariadb.jdbc.Driver");
		String dburl = "jdbc:mariadb://127.0.0.1:3307/test";
		String dbuser = "root";
		String dbpw = "java1004";

		Connection conn = DriverManager.getConnection(dburl, dbuser, dbpw);

		String sql = "SELECT * FROM member WHERE member_id=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, memberId);
		ResultSet rs = stmt.executeQuery();

		member = new Member();

		if (rs.next()) {
			member.memberId = rs.getString("member_id");
			member.memberPw = rs.getString("member_pw");
			member.memberName = rs.getString("member_name");
		}

		rs.close();
		stmt.close();
		conn.close();

		return member;
	}

	// member 전체목록을 출력
	// public ResultSet selectMemberList() throws ClassNotFoundException,
	// SQLException {
	// ResultSet처럼 특정 라이브러리에 의존하는 타입을 리턴하는 것은 좋지 못하다.
	// 일반적인 타입의 리턴이 필요 List or 배열
	public ArrayList<Member> selectMemberList(int currentPage) throws ClassNotFoundException, SQLException {
		// DB
		Class.forName("org.mariadb.jdbc.Driver");
		String dburl = "jdbc:mariadb://127.0.0.1:3307/test";
		String dbuser = "root";
		String dbpw = "java1004";

		Connection conn = DriverManager.getConnection(dburl, dbuser, dbpw);
		
		int rowPerPage = 10;
		int beginRow = (currentPage - 1) * rowPerPage;

		String sql = "SELECT * FROM member LIMIT ?, ?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, beginRow);
		stmt.setInt(2, rowPerPage);
		ResultSet rs = stmt.executeQuery();

		System.out.println("[member_id]\t[member_pw]\t[member_name]");

		ArrayList<Member> list = new ArrayList<>();
		while (rs.next()) {
			Member m = new Member();
			m.memberId = rs.getString("member_id");
			m.memberPw = rs.getString("member_pw");
			m.memberName = rs.getString("member_name");
			list.add(m);
		}

		// DB자원 해제 코드
		rs.close();
		stmt.close();
		conn.close();

		return list;
	}

	public boolean insertMember(Member member) throws ClassNotFoundException, SQLException {
		// DB
		Class.forName("org.mariadb.jdbc.Driver");
		String dburl = "jdbc:mariadb://127.0.0.1:3307/test";
		String dbuser = "root";
		String dbpw = "java1004";

		Connection conn = DriverManager.getConnection(dburl, dbuser, dbpw);

		String sql = "INSERT INTO member(member_id, member_pw, member_name) VALUES(?, ?, ?)";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, member.memberId);
		stmt.setString(2, member.memberPw);
		stmt.setString(3, member.memberName);

		int success = stmt.executeUpdate();

		System.out.println("");
		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean updateMember(String memberId, String memberPw, String memberPwNew)
			throws ClassNotFoundException, SQLException {
		// DB
		Class.forName("org.mariadb.jdbc.Driver");
		String dburl = "jdbc:mariadb://127.0.0.1:3307/test";
		String dbuser = "root";
		String dbpw = "java1004";

		Connection conn = DriverManager.getConnection(dburl, dbuser, dbpw);

		String sql = "UPDATE member SET member_pw=? WHERE member_id=? AND member_pw=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, memberPwNew);
		stmt.setString(2, memberId);
		stmt.setString(3, memberPw);

		int success = stmt.executeUpdate();

		System.out.println("");

		stmt.close();
		conn.close();

		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean deleteMember(Member member) throws ClassNotFoundException, SQLException {
		// DB
		Class.forName("org.mariadb.jdbc.Driver");
		String dburl = "jdbc:mariadb://127.0.0.1:3307/test";
		String dbuser = "root";
		String dbpw = "java1004";

		Connection conn = DriverManager.getConnection(dburl, dbuser, dbpw);

		String sql = "DELETE FROM member WHERE member_id=? AND member_pw=?";
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, member.memberId);
		stmt.setString(2, member.memberPw);

		int success = stmt.executeUpdate();


		stmt.close();
		conn.close();
		
		if (success == 1) {
			return true;
		} else {
			return false;
		}
	}
}

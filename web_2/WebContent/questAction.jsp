<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
	// String s = request.getParameter("quest"); -> 단 하나의 문자열 값만 받을 수있다.
	String[] quest = request.getParameterValues("quest");
	System.out.println(quest); // quest배열의 참조값 출력
	int total = 0;
	for (int i = 0; i < quest.length; i += 1) {
		total = total + Integer.parseInt(quest[i]);
	}
	%>
	<div>
		합계 :
		<%=total%></div>
	<div>
		평균 :
		<%=total / quest.length%></div>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<h1>K Pop 온라인 시험</h1>
	<h2>1~5번 : 보기에서 고르시오.</h2>

	<h3>보기</h3>
	<p>RM, 진, 슈가, 제이홉, 지민, 뷔, 정국, 지수, 제니, 로제, 이특, 희철, 예성, 신동, 은혁, 동해,
		시원, 려욱, 규현, 성민, 나연, 정연, 모모, 사나, 지효, 미나, 다현, 채영, 쯔위, 청하</p>
	<br>
	<form action="./kpopAction.jsp">
		<p>1. BTS 리더를 보기에서 고르시오</p>
		<input type="text" name="question">
		<p>2. 블랙핑크의 외국인 멤버를 고르시오</p>
		<input type="text" name="question">
		<p>3. 슈퍼주니어의 리더를 보기에서 고르시오</p>
		<input type="text" name="question">
		<p>4. 트와이스의 리더를 보기에서 고르시오</p>
		<input type="text" name="question">
		<p>5. 솔로 여자가수를 보기에서 고르시오</p>
		<input type="text" name="question">
		<br>
		<button type="submit" style="margin-top: 10px;">제출</button>
	</form>

</body>
</html>
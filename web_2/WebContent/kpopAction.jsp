<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
	String[] answer = { "RM", "리사", "이특", "지효", "청하" };
	String[] question = request.getParameterValues("question");
	int score = 0;

	for (int i = 0; i < question.length; i++) {
		if (question[i].equals(answer[i])) {
			score += 20;
		}
	}
	%>
	<div><%=score%></div>
	<%
	for (int i = 0; i < 10; i++) {
		if (i < score / 10) {
	%>
	★
	<%
	} else {
	%>
	☆
	<%
	}
	}
	%>
</body>
</html>
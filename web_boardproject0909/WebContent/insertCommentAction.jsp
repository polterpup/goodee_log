<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>insertCommentAction.jsp</title>
</head>
<body>
<%
	request.setCharacterEncoding("utf-8");
	if(request.getParameter("boardNo") == null) {
		response.sendRedirect("./selectBoardList.jsp");
		return;
	}
	int boardNo=Integer.parseInt(request.getParameter("boardNo"));
	System.out.println(boardNo+" <-- boardNo");		
	
	// insert into comment(board_no, comment_cotent, comment_date) values(?, ?, now()) 
	// 실행 코드 구현
	
	response.sendRedirect("./selectBoardOne.jsp?boardNo="+boardNo);
%>
</body>
</html>
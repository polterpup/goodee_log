<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "vo.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<%
	request.setCharacterEncoding("utf-8");
	
	if(request.getParameter("boardNo") == null) {
		response.sendRedirect("./selectBoardList.jsp");
		return;
	}
	
	int boardNo=Integer.parseInt(request.getParameter("boardNo"));
	System.out.println(boardNo+" <-- boardNo");
	
	ArrayList<String> boardCategoryList = new ArrayList<>();	
	
	Class.forName("org.mariadb.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.01:3306/boardproject", "root", "java1004");
	String sql = "select distinct board_category from board order by board_category asc";
	PreparedStatement stmt = conn.prepareStatement(sql);
	ResultSet rs = stmt.executeQuery();
	while(rs.next()) {
		boardCategoryList.add(rs.getString("board_category"));
	}
%>	
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a class="navbar-brand btn btn-warning" href="./selectBoardList.jsp">전체보기</a>
		<%
			for(String category : boardCategoryList) {
		%>
				<a class="navbar-brand" href="./selectBoardList.jsp?boardCategory=<%=category%>"><%=category%></a>
		<%		
			}
		%>
	</nav>
	<div class="jumbotron">
		<h1>게시글 상세보기</h1>
	</div>
	<%
		String sql2 = "select board_category, board_title, board_content, board_date from board where board_no=?";
		PreparedStatement stmt2 = conn.prepareStatement(sql2);
		stmt2.setInt(1, boardNo);
		ResultSet rs2 = stmt2.executeQuery();
		Board board = null;
		if(rs2.next()) {
			board = new Board();
			board.boardCategory = rs2.getString("board_category");
			board.boardTitle = rs2.getString("board_title");
			board.boardContent = rs2.getString("board_content");
			board.boardDate = rs2.getString("board_date");
		}
	%>	
	<div>
		<table class="table table-dark">
			<tr>
				<td>board_no</td>
				<td>
					<div><%=boardNo%></div>
					<div><a class="btn-secondary" href="./updateBoardForm.jsp?boardNo=<%=boardNo%>">수정</a></div>
					<div><a class="btn-secondary" href="./deleteBoard.jsp?boardNo=<%=boardNo%>">삭제</a></div>
				</td>
			</tr>
			<tr>
				<td>board_category</td>
				<td><%=board.boardCategory%></td>
			</tr>
			<tr>
				<td>board_title</td>
				<td><%=board.boardTitle%></td>
			</tr>
			<tr>
				<td>board_content</td>
				<td><%=board.boardContent%></td>
			</tr>
			<tr>
				<td>board_date</td>
				<td><%=board.boardDate%></td>
			</tr>
		</table>
	</div>
	<!-- 댓글 partial -->
	<h3>댓글입력</h3>
	<div>
		<form action="./insertCommentAction.jsp" method="post">
			<input type="hidden" name="boardNo" value="<%=boardNo%>">
			<div class="form-group">
				<label for="comment">Comment:</label>
				<textarea name="commentContent" class="form-control" rows="5"></textarea>
			</div>
			<button class="btn btn-warning" type="submit">댓글입력</button>
		</form>
	</div>
	<h3>댓글목록</h3>
	<div>
		<!-- 댓글목록 출력, 10개씩 페이징 -->
		<%
			ArrayList<Comment> commentList = new ArrayList<>();
			int commentCurrentPage = 1;
			int commentRowPerPage = 10;
			int comentBegingRow = (commentCurrentPage-1) * commentRowPerPage;
			String sql3 = "select comment_no, comment_content from comment where board_no=? order by comment_date desc limit ?,?";
			PreparedStatement stmt3 = conn.prepareStatement(sql3);
			stmt3.setInt(1, boardNo);
			stmt3.setInt(2, comentBegingRow);
			stmt3.setInt(3, commentRowPerPage);
			ResultSet rs3 = stmt3.executeQuery();
			while(rs3.next()) {
				Comment c = new Comment();
				c.commentNo = rs3.getInt("comment_no");
				c.commentContent = rs3.getString("comment_content");
				commentList.add(c);
			}
		
			for(Comment c : commentList) {
			%>
				<div class="bg-dark text-white">
					<%=c.commentContent%>
					<a href="./deleteComment.jsp?commentNo=<%=c.commentNo%>&boardNo=<%=boardNo%>" class="btn-secondary">삭제</a>
					<!-- commentNo의 덧글을 삭제 후 다시 이 페이지 되돌아오기(redirect) 위해서 boardNo 넘겨준다 -->
				</div>
			<%	
			}
		%>
		<div>
			<a href="">이전</a>
			<a href="">다음</a>
		</div>		
		
	</div>
</div>
</body>
</html>
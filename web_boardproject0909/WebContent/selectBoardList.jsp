<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*" %>
<%@ page import = "java.util.ArrayList" %>
<%@ page import = "vo.Board" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<%
	request.setCharacterEncoding("utf-8");
	int currentPage = 1;
	if(request.getParameter("currentPage") != null) { 
		currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}
	
	String boardCategory = "";
	if(request.getParameter("boardCategory") != null) {
		boardCategory = request.getParameter("boardCategory");
	}
	// boardCategory는 null or "csharp", "css", ....
	// null이면 전체에서 select
	// null아 아니면 boardCategory만 select
 	
	
	ArrayList<String> boardCategoryList = new ArrayList<>();	

	Class.forName("org.mariadb.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.01:3306/boardproject", "root", "java1004");
	String sql = "select distinct board_category from board order by board_category asc";
	PreparedStatement stmt = conn.prepareStatement(sql);
	ResultSet rs = stmt.executeQuery();
	while(rs.next()) {
		boardCategoryList.add(rs.getString("board_category"));
	}

%>
	<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
		<a class="navbar-brand btn btn-warning" href="./selectBoardList.jsp">전체보기</a>
		<%
			for(String category : boardCategoryList) {
		%>
				<a class="navbar-brand" href="./selectBoardList.jsp?boardCategory=<%=category%>"><%=category%></a>
		<%		
			}
		%>
	</nav>
	
	<div>
		<!-- 본문내용 -->
		<%
			ArrayList<Board> boardList = new ArrayList<>();	
			
			// boardCategory값에 따라 분기 -> 동적쿼리
			String sql2 = null;
			PreparedStatement stmt2 = null;
			// !boardCategory.equals("") , boardCategory.equals("")!=true , boardCategory.equals("")==false
			if(boardCategory.equals("") == true) { 
				sql2 = "select board_no, board_category, board_title from board order by board_date desc limit ?,?";
				stmt2 = conn.prepareStatement(sql2);
				stmt2.setInt(1, 0);
				stmt2.setInt(2, 10);
			} else {
				sql2 = "select board_no, board_category, board_title from board where board_category=? order by board_date desc limit ?,?";
				stmt2 = conn.prepareStatement(sql2);
				stmt2.setString(1, boardCategory);
				stmt2.setInt(2, 0);
				stmt2.setInt(3, 10);
			}
		
			ResultSet rs2 = stmt2.executeQuery();
			// ResultSet 특수한 타입에서 ArrayList라는 일반화된 타입으로 변환(가공)
			while(rs2.next()) {
				Board board = new Board();
				board.boardNo = rs2.getInt("board_no");
				board.boardCategory = rs2.getString("board_category");
				board.boardTitle = rs2.getString("board_title");
				boardList.add(board);
			}
		%>
		
		<div class="jumbotron">
			<h1>전체 게시글 목록</h1>
			<p>
				csharp, html, java,...
				<div><a href="./insertBoardForm.jsp" class="btn btn-warning">글입력</a></div>
			</p>
		</div>
		
		<table class="table table-dark table-striped">
			<thead>
				<tr>
					<th>board_category</th>
					<th>board_title</th>
				</tr>
			</thead>
			<tbody>
				<%
					for(Board b : boardList) {
				%>
						<tr>
							<td><%=b.boardCategory%></td>
							<!-- 제목링크 상세보기.... -->
							<td><a href="./selectBoardOne.jsp?boardNo=<%=b.boardNo%>"><%=b.boardTitle%></a></td>
						</tr>
				<%		
					}
				%>
			</tbody>
		</table>
		<div>
			<a class="btn btn-warning" href="">이전</a>
			<a class="btn btn-warning" href="./selectBoardList.jsp?currentPage=<%=currentPage+1%>&boardCategory=<%=boardCategory%>">다음</a>
		</div>
	</div>
</div>
</body>
</html>




<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- submit버튼을 클릭하면 <form>태그의 값(""들)이
	action 속성의 프로그램으로 넘겨진다 -->
	<h1>로그인</h1>
	<form action="./loginAction.jsp">
		<table border="1"> 
		
			<tr>
				<td>아이디</td>
				<td><input type="text" name="id"></td>
			</tr>
			<tr>
				<td>비밀번호</td>
				<td><input type="password" name="pw"></td>
			</tr>
		</table>
		<button type="submit">로그인</button>
	
	</form>

</body>
</html>
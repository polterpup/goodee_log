<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<form action="./testAction.jsp">

	<h2>정보처리기사 필기</h2>
	
	<fieldset>
		<legend>1. 검토회의 전에 요규사항 명세서를 미리 배포하여 사전 검토한 후 짧은 검토 회의를 통해 오류를 조기에 검출하는데 목적을 두는 요구 사항 검토 방법은?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q1" value="A1"><label>1. 빌드검증</label></div>
					<div></div><input type="radio" name="Q1" value="A1"><label>2. 동료 검토</label></div>
					<div></div><input type="radio" name="Q1" value="A1"><label>3. 워크 스루</label></div>
					<div></div><input type="radio" name="Q1" value="A1"><label>4. 개발자 검토</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>2. 코드 설계에서 일정한 일련번호를 부여하는 방식의 코드는?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q2" value="A2"><label>1. 연상 코드</label></div>
					<div></div><input type="radio" name="Q2" value="A2"><label>2. 블록 코드</label></div>
					<div></div><input type="radio" name="Q2" value="A2"><label>3. 순차 코드</label></div>
					<div></div><input type="radio" name="Q2" value="A2"><label>4. 표의 숫자 코드</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>3. 객체지향 프로그램에서 데이터를 추상화하는 단위는?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q3" value="A3"><label>1. 메소드</label></div>
					<div></div><input type="radio" name="Q3" value="A3"><label>2. 클래스</label></div>
					<div></div><input type="radio" name="Q3" value="A3"><label>3. 상속성</label></div>
					<div></div><input type="radio" name="Q3" value="A3"><label>4. 메시지</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>4. 데이터 흐름도(DFD)의 구성요소에 포함되지 않는 것은?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q4" value="A4"><label>1. process</label></div>
					<div></div><input type="radio" name="Q4" value="A4"><label>2. data flow</label></div>
					<div></div><input type="radio" name="Q4" value="A4"><label>3. data store</label></div>
					<div></div><input type="radio" name="Q4" value="A4"><label>4. data dictionary</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>5. 소프트웨어 설계시 구축된 플랫폼의 성능특성 분석에 사용되는 측정 항목이 아닌 것은?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q5" value="A5"><label>1. 응답시간(Response Time)</label></div>
					<div></div><input type="radio" name="Q5" value="A5"><label>2. 가용성(Availability)</label></div>
					<div></div><input type="radio" name="Q5" value="A5"><label>3. 사용률(Utilization)</label></div>
					<div></div><input type="radio" name="Q5" value="A5"><label>4. 서버 튜닝(Server Tuning)</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>6. UML확장 모델에서 스테레오 타입 객체를 표현할 때 사용하는 기호로 맞는 것은?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q6" value="A6"><label>1. << >></label></div>
					<div></div><input type="radio" name="Q6" value="A6"><label>2. (( ))</label></div>
					<div></div><input type="radio" name="Q6" value="A6"><label>3. {{ }}</label></div>
					<div></div><input type="radio" name="Q6" value="A6"><label>4. [[ ]]</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>7. GOF(Gang of Four)의 디자인 패턴에서 행위 패턴에 속하는 것은?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q7" value="A7"><label>1. Builder</label></div>
					<div></div><input type="radio" name="Q7" value="A7"><label>2. Visitor</label></div>
					<div></div><input type="radio" name="Q7" value="A7"><label>3. Prototype</label></div>
					<div></div><input type="radio" name="Q7" value="A7"><label>4. Bridge</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>8. 자료 사전에서 자료의 생략을 의미하는 기호는?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q8" value="A8"><label>1. {}</label></div>
					<div></div><input type="radio" name="Q8" value="A8"><label>2. **</label></div>
					<div></div><input type="radio" name="Q8" value="A8"><label>3. =</label></div>
					<div></div><input type="radio" name="Q8" value="A8"><label>4. ()</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>9. 트랜잭션이 올바르게 처리되고 있는지 데이터를 감시하고 제어하는 미들웨어는?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q9" value="A9"><label>1. RPC</label></div>
					<div></div><input type="radio" name="Q9" value="A9"><label>2. ORB</label></div>
					<div></div><input type="radio" name="Q9" value="A9"><label>3. TP monitor</label></div>
					<div></div><input type="radio" name="Q9" value="A9"><label>4. HUB</label></div>
					</td>
				</tr>
			</table>
	</fieldset>
	<fieldset>
		<legend>10. UI 설계 원칙에서 누구나 쉽게 이해하고 사용할 수 있어야 한다는 것은?</legend>
			<table>
				<tr>
					<td colspan='2'>
					<div></div><input type="radio" name="Q10" value="A10"><label>1. 유효성</label></div>
					<div></div><input type="radio" name="Q10" value="A10"><label>2. 직관성</label></div>
					<div></div><input type="radio" name="Q10" value="A10"><label>3. 무결성</label></div>
					<div></div><input type="radio" name="Q10" value="A10"><label>4. 유연성</label></div>
					</td>
				</tr>
			</table>
			
	</fieldset>
			<table>
				<tr>
					<td><button type="submit">답안전송</button></td>
				</tr>
			</table>




</form>

</body>
</html>
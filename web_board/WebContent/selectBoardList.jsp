<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="vo.Board"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Board List</h1>
	<%
	int currentPage = 1;
	if (request.getParameter("currentPage") != null) {
		currentPage = Integer.parseInt(request.getParameter("currentPage"));
	}
	System.out.println(currentPage + " <-- currentPage");

	int rowPerPage = 10;
	int beginRow = (currentPage - 1) * rowPerPage;

	// 1) 데이터수집
	Class.forName("org.mariadb.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/webboard", "root", "java1004");
	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM board order by board_date desc limit ?, ?");
	stmt.setInt(1, beginRow);
	stmt.setInt(2, rowPerPage);

	System.out.println(stmt + " <-- stmt"); // Debug
	ResultSet rs = stmt.executeQuery();

	// 2) 데이터가공 (ResultSet --> Board[] or ArrayList<Board>)
	// 빈 ArrayList 생성
	ArrayList<Board> list = new ArrayList<>();
	while (rs.next()) {
		Board b = new Board();
		b.boardNo = rs.getInt("board_no");
		b.boardTitle = rs.getString("board_title");
		b.boardUser = rs.getString("board_user");
		list.add(b);
	}

	// 3) 이행(출력)
	%>
	<table border="1" cellpadding="5" cellspacing="5"
		style="text-align: center;">
		<thead>
			<tr>
				<th>boardNo</th>
				<th>boardTitle</th>
				<th>boardUser</th>
			</tr>
		</thead>
		<tbody>
			<%
			for (Board b : list) {
			%>
			<tr>
				<td><%=b.boardNo%></td>
				<td><a href="./selectBoardOne.jsp?boardNo=<%=b.boardNo%>"><%=b.boardTitle%></a></td>
				<td><%=b.boardUser%></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
	<br>
	<%
	if (currentPage > 1) {
	%>
	<a href="./selectBoardList.jsp?currentPage=<%=currentPage - 1%>">이전</a>
	<%
	}
	int totalCount = 0;
	PreparedStatement stmt2 = conn.prepareStatement("select count(*) from board");
	ResultSet rs2 = stmt2.executeQuery();
	
	if (rs2.next()) {
		totalCount = rs2.getInt("count(*)");
	}

	int lastPage = totalCount / rowPerPage;
	
	if (totalCount % rowPerPage != 0) {
	lastPage += 1; // lastPage++; lastPage=lastPage+1;
	}

	if (currentPage < lastPage) {
	%>
	<a href="./selectBoardList.jsp?currentPage=<%=currentPage + 1%>" style="margin-left: 260px">다음</a>
	<%
	}
	%>
</body>
</html>

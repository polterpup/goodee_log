<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="vo.Board"%>
<%@ page import="java.sql.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
	// 0.설정
	request.setCharacterEncoding("utf-8");

	// 1. 데이터 수집
	String boardPw = request.getParameter("boardPw");
	String boardTitle = request.getParameter("boardTitle");
	String boardContent = request.getParameter("boardContent");
	String boardUser = request.getParameter("boardUser");

	/*
	if (boardPw == null || boardTitle == null || boardContent == null || boardUser == null) {
		response.sendRedirect("./insertBoardForm.jsp");
		return; // 메서드를 종료하는 명령어
	}
	*/

	// 디버깅
	System.out.println(boardPw + " <-- boardPw");
	System.out.println(boardTitle + " <-- boardTitle");
	System.out.println(boardContent + " <-- boardContent");
	System.out.println(boardUser + " <-- boardUser");

	// 2. 데이터 가공(자료구조)
	Board board = new Board();
	board.boardPw = boardPw;
	board.boardTitle = boardTitle;
	board.boardContent = boardContent;
	board.boardUser = boardUser;

	// 3. 이행(DB에 입력)
	// 3-1) mariadb 사용
	Class.forName("org.mariadb.jdbc.Driver");
	System.out.println("mariadb Driver 등록성공!");
	// 3-2) 접속
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/webboard", "root", "java1004");
	System.out.println(conn + " <-- conn");
	// 3-3) 쿼리생성, 실행
	PreparedStatement stmt = conn.prepareStatement(
			"insert into board(board_pw, board_title, board_content, board_user, board_date) values(?,?,?,?,now())");
	stmt.setString(1, board.boardPw);
	stmt.setString(2, board.boardTitle);
	stmt.setString(3, board.boardContent);
	stmt.setString(4, board.boardUser);
	System.out.println(stmt + " <-- conn"); // stmt출력시 쿼리내용 같이 출력
	int row = stmt.executeUpdate();

	if (row == 1) {
		response.sendRedirect("./selectBoardList.jsp?currentPage=1");

	} else {
		System.out.println("잘못된 입력입니다.");
	}
	%>
</body>
</html>








<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.sql.*"%>
<%@ page import="java.util.*"%>
<%@ page import="vo.Board"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
</head>
<body>
	<%
	int boardNo = Integer.parseInt(request.getParameter("boardNo"));
	System.out.println(boardNo + " <-- boardNo");

	/*
		1. 수집
		
		select board_no, board_title, board_content, board_user, board_date
		from board
		where board_no = ?
		
		stmt.setInt(1, boardNo);

		2.가공
		
		Board board = null;
		
		if(rs.next()) {
			board = new Board();	
		}
		
		3. board 출력
	*/

	// DB
	Class.forName("org.mariadb.jdbc.Driver");
	Connection conn = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3307/webboard", "root", "java1004");
	PreparedStatement stmt = conn.prepareStatement("SELECT * FROM board where board_no=?");
	stmt.setInt(1, boardNo);

	System.out.println(stmt + " <-- stmt"); // Debug
	ResultSet rs = stmt.executeQuery();

	// 데이터가공
	ArrayList<Board> list = new ArrayList<>();
	while (rs.next()) {
		Board b = new Board();
		b.boardNo = rs.getInt("board_no");
		b.boardTitle = rs.getString("board_title");
		b.boardContent = rs.getString("board_Content");
		b.boardUser = rs.getString("board_user");
		b.boardDate = rs.getString("board_date");
		list.add(b);
	}

	// 출력
	%>
	<table border="1">
		<thead>
			<tr>
				<th>boardNo</th>
				<th>boardTitle</th>
				<th>boardContent</th>
				<th>boardUser</th>
				<th>boardDate</th>
			</tr>
		</thead>
		<tbody>
			<%
			for (Board b : list) {
			%>
			<tr>
				<td><%=b.boardNo%></td>
				<td><%=b.boardTitle%></td>
				<td><%=b.boardContent%></td>
				<td><%=b.boardUser%></td>
				<td><%=b.boardDate%></td>
			</tr>
			<%
			}
			%>
		</tbody>
	</table>
</body>
</html>